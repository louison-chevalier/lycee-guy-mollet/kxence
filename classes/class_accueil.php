<?php

class Accueil {
    
    private $derniereCo;
    private $selectOne;
    private $selOne;
    private $seOne;
    
     public function __construct($db){
        $this->derniereCo = $db->prepare("SELECT DISTINCT(pseudo), MAX(DATE) as date FROM connexion  GROUP BY pseudo ORDER BY date desc");
        $this->selectOne = $db->prepare("select * from connexion where pseudo=:pseudo");
        $this->selOne = $db->prepare("select * from admin where pseudo=:pseudo");
        $this->seOne = $db->prepare("select * from professeur where pseudo=:pseudo");
    }
    
    
    public function derniereCo(){
        $this->derniereCo->execute();
        return $this->derniereCo->fetchAll();
    }
    
    public function selectOne($pseudo){ 
        $this->selectOne->execute(array(':pseudo'=>$pseudo)); 
        return $this->selectOne->fetch();
    }
    
    public function selOne($pseudo){ 
        $this->selOne->execute(array(':pseudo'=>$pseudo)); 
        return $this->selOne->fetch();
    }
    
    public function seOne($pseudo){ 
        $this->seOne->execute(array(':pseudo'=>$pseudo)); 
        return $this->seOne->fetch();
    }
}


?>