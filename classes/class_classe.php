<?php

class Classe{
    
    private $selectAnnee;
    private $listeAnnee;
    private $insertAll;
    private $insert;
    private $selectAll;
    private $deleteOne;
    private $selectOne;
    private $updateAll;
    private $selectNum;
    
    
    public function __construct($db){
        $this->selectAll = $db->prepare("SELECT * FROM classe");
        $this->selectAnnee = $db->prepare("SELECT * FROM classe WHERE anneeDeb=:anneeDeb");
        $this->selectNum = $db->prepare("SELECT * FROM classe WHERE nomClasse=:nomClasse and anneeDeb= :anneeDeb and anneeFin=:anneeFin");
        $this->listeAnnee = $db->prepare("SELECT DISTINCT anneeDeb FROM classe ORDER BY anneeDeb");
        $this->insertAll = $db->prepare("INSERT INTO classe (nomClasse, anneeDeb, anneeFin) VALUES ( :nomClasse, :anneeDeb, :anneeFin)");
        $this->insert = $db->prepare("INSERT INTO classe (numClasse, nomClasse, anneeDeb, anneeFin) VALUES ( :numClasse, :nomClasse, :anneeDeb, :anneeFin)");
        $this->deleteOne = $db->prepare("delete from classe where numClasse=:numClasse") ;
        $this->selectOne = $db->prepare("select * from classe where numClasse=:numClasse");
        $this->updateAll = $db->prepare("update classe set nomClasse=:nomClasse, anneeDeb=:anneeDeb, anneeFin=:anneeFin where numClasse=:numClasse") ; 
    }
     
    public function selectAnnee($anneeDeb){
        $this->selectAnnee->execute(array(':anneeDeb'=>$anneeDeb)); 
        return $this->selectAnnee->fetchAll();
    }   
    
    public function listeAnnee(){
        $this->listeAnnee->execute(); 
        return $this->listeAnnee->fetchAll();
    }   
    
    public function insertAll($nomClasse, $anneeDeb , $anneeFin){
        $this->insertAll->execute(array(':nomClasse'=>$nomClasse,':anneeDeb' => $anneeDeb,':anneeFin' => $anneeFin));
        return $this->insertAll->rowCount();
    }
    
     public function insert($numClasse, $nomClasse, $anneeDeb , $anneeFin){
        $this->insert->execute(array(':numClasse'=>$numClasse, ':nomClasse'=>$nomClasse,':anneeDeb' => $anneeDeb,':anneeFin' => $anneeFin));
        return $this->insert->rowCount();
    }
    
    public function selectAll(){
        $this->selectAll->execute();
        return $this->selectAll->fetchAll();
    }
    
     public function deleteOne($numClasse){
        $this->deleteOne->execute(array(':numClasse'=>$numClasse));
        return $this->deleteOne->rowCount();
    }
    
     public function selectOne($numClasse){ 
        $this->selectOne->execute(array(':numClasse'=>$numClasse)); 
        return $this->selectOne->fetch();
    }
    
    public function selectNum($nomClasse, $anneeDeb, $anneeFin){ 
        $this->selectNum->execute(array(':nomClasse'=>$nomClasse, ':anneeDeb'=>$anneeDeb, ':anneeFin'=>$anneeFin)); 
        return $this->selectNum->fetch();
    }
    
    public function updateAll($numClasse, $nomClasse, $anneeDeb, $anneeFin){
        $this->updateAll->execute(array(':numClasse'=>$numClasse, ':nomClasse'=>$nomClasse, ':anneeDeb'=>$anneeDeb, ':anneeFin'=>$anneeFin));
        return $this->updateAll->rowCount();
    }
}

?>