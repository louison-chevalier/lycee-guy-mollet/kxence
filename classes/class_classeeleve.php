<?php

class Classeeleve{
    
    private $selectEleve;
    private $selectnoEleve;
    private $insertAll;
    private $deleteOne;


    public function __construct($db){
        $this->insertAll = $db->prepare("INSERT INTO classeeleve (pseudo, numClasse) VALUES (:pseudo, :numClasse)");
        $this->selectEleve = $db->prepare("SELECT * FROM eleve WHERE pseudo in (SELECT pseudo FROM classeeleve WHERE numClasse=:numClasse)");
        $this->selectnoEleve = $db->prepare("SELECT * FROM eleve WHERE pseudo NOT IN (SELECT pseudo FROM classeeleve WHERE numClasse=:numClasse)");
        $this->deleteOne = $db->prepare("delete from classeEleve where numClasse=:numClasse") ;
    }
    
    public function selectEleve($numClasse){
        $this->selectEleve->execute(array(':numClasse'=>$numClasse)); 
        return $this->selectEleve->fetchAll();
    }
    
    public function selectnoEleve($numClasse){
        $this->selectnoEleve->execute(array(':numClasse'=>$numClasse)); 
        return $this->selectnoEleve->fetchAll();
    }   
    
    public function insertAll($pseudo, $numClasse){
        $this->insertAll->execute(array(':pseudo'=>$pseudo, ':numClasse'=>$numClasse));
        return $this->insertAll->rowCount();
    }

    public function deleteOne($numClasse){
        $this->deleteOne->execute(array(':numClasse'=>$numClasse));
        return $this->deleteOne->rowCount();
    }
    
}

?>