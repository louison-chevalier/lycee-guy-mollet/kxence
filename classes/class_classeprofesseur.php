<?php

class Classeprofesseur{
    
    private $selectProf;
    private $insertAll;
    private $selection;
    private $selectnoProf;
    
    public function __construct($db){
        $this->insertAll = $db->prepare("INSERT INTO classeprofesseur (pseudo, numClasse) VALUES (:pseudo, :numClasse)");
        $this->selectProf = $db->prepare("SELECT * FROM professeur WHERE pseudo in (SELECT pseudo FROM classeprofesseur WHERE numClasse=:numClasse)");
        $this->selection = $db->prepare("SELECT * FROM classeprofesseur WHERE  pseudo=:pseudo and numClasse= :numClasse ");
        $this->selectnoProf = $db->prepare("SELECT * FROM professeur WHERE pseudo NOT IN (SELECT pseudo FROM classeprofesseur WHERE numClasse=:numClasse)");
        
        
    }
    
    public function selection($numClasse){
        $this->selection->execute(array(':pseudo'=>$pseudo, ':numClasse'=>$numClasse)); 
        return $this->selection->fetchAll();
    }   
    
    
    public function selectProf($numClasse){
        $this->selectProf->execute(array(':numClasse'=>$numClasse)); 
        return $this->selectProf->fetchAll();
    }   
    
    public function selectnoProf($numClasse){
        $this->selectnoProf->execute(array(':numClasse'=>$numClasse)); 
        return $this->selectnoProf->fetchAll();
    }   
    
    
    public function insertAll($pseudo, $numClasse){
        $this->insertAll->execute(array(':pseudo'=>$pseudo, ':numClasse'=>$numClasse));
        return $this->insertAll->rowCount();
    }
}

?>