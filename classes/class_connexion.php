<?php class Connexion {

    private $insertAll;
    private $deleteOne;
    private $countpseudo;
    private $selectAll;
    private $selectminco;

    // Constructeur
    // Paramètre : Base de données
    
    
    public function __construct($db){
        $this->insertAll = $db->prepare("INSERT INTO connexion (pseudo, role, remoteip, navigateur) VALUES (:pseudo, :role, :remoteip, :navigateur)");
        $this->deleteOne = $db->prepare("delete from admin where pseudo=:pseudo") ; 
        $this->countpseudo = $db->prepare("SELECT count(pseudo) FROM connexion WHERE pseudo=:pseudo");
        $this->selectminco = $db->prepare("SELECT date FROM connexion WHERE pseudo=:pseudo ORDER BY date DESC LIMIT 2");
        $this->selectAll = $db->prepare("SELECT * FROM connexion order by date desc");
        
    }
    
    public function selectAll(){
        $this->selectAll->execute();
        return $this->selectAll->fetchAll();
    }
    
    public function insertAll($pseudo, $role, $remoteip, $navigateur){
        $this->insertAll->execute(array(':pseudo'=>$pseudo, ':role'=>$role, ':remoteip'=>$remoteip, ':navigateur'=>$navigateur));
        return $this->insertAll->rowCount();
    }

     public function countpseudo($pseudo){
        $this->countpseudo->execute(array(':pseudo'=>$pseudo));
        return $this->countpseudo->fetchColumn();
    }
    
    public function selectminco($pseudo){
        $this->selectminco->execute(array(':pseudo'=>$pseudo));
        return $this->selectminco->fetchAll();
    }
}

?>