<?php class Eleve {

    private $insertAll;
    private $deleteOne;
    private $selectAll;
    private $selectOne; 
    private $selectConnexion;
    private $updateAll;
    private $mdpoublie;

    // Constructeur
    // Paramètre : Base de données
    
    
    public function __construct($db){
        $this->insertAll = $db->prepare("INSERT INTO eleve (pseudo, mail, nom, prenom, mdp, photo, anniversaire) VALUES (:pseudo, :mail, :nom, :prenom, :mdp, :photo, :anniversaire)");
        $this->deleteOne = $db->prepare("delete from eleve where pseudo=:pseudo") ; 
        $this->selectAll = $db->prepare("SELECT * FROM eleve");
        $this->mdpoublie = $db->prepare("SELECT mail FROM eleve WHERE pseudo=:pseudo and anniversaire=:anniversaire");
        $this->selectOne = $db->prepare("select * from eleve where pseudo=:pseudo");
        $this->selectConnexion = $db->prepare("SELECT pseudo, mdp from eleve where pseudo = :pseudo and mdp= :mdp ");
        $this->updateAll = $db->prepare("update eleve set mail=:mail, nom=:nom, prenom=:prenom, mdp=:mdp, photo=:photo, anniversaire=:anniversaire where pseudo=:pseudo") ; 
    }
     
    
    public function insertAll($pseudo, $mail, $nom , $prenom, $mdp, $photo, $anniversaire){
        $this->insertAll->execute(array(':pseudo'=>$pseudo, ':mail'=>$mail,':nom' => $nom,':prenom' => $prenom,':mdp' => $mdp, ':photo' => $photo, ':anniversaire' => $anniversaire));
        return $this->insertAll->rowCount();
    }


    public function selectConnexion($pseudo, $mdp){
        $this->selectConnexion->execute(array(':pseudo'=>$pseudo,':mdp' => $mdp));
        return $this->selectConnexion->fetch();
    }
  

    public function selectAll(){
        $this->selectAll->execute();
        return $this->selectAll->fetchAll();
    }

    public function selectOne($pseudo){ 
        $this->selectOne->execute(array(':pseudo'=>$pseudo)); 
        return $this->selectOne->fetch();
    }
    
    public function mdpoublie($pseudo, $anniversaire){ 
        $this->mdpoublie->execute(array(':pseudo'=>$pseudo, ':anniversaire'=>$anniversaire)); 
        return $this->mdpoublie->fetch();
    }

    public function deleteOne($pseudo){
        $this->deleteOne->execute(array(':pseudo'=>$pseudo));
        return $this->deleteOne->rowCount();
    }


    public function updateAll($pseudo, $mail, $nom, $prenom, $mdp, $photo, $anniversaire){
        $this->updateAll->execute(array(':pseudo'=>$pseudo, ':mail'=>$mail, ':nom'=>$nom, ':prenom'=>$prenom, ':mdp'=>$mdp, ':photo'=>$photo, ':anniversaire'=>$anniversaire));
        return $this->updateAll->rowCount();
    }
}

?>