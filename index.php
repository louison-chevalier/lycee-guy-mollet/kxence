<?php
session_start();

// Appel de la page qui gère les accès aux pages
require_once 'src/listePages.php';


// Appel de la page qui gènére des mots de passe
require_once 'src/mdp.php';


// Appel de la page qui censure adresse mail
require_once 'src/censure.php';

// Appel de la page qui gère l'entete, le menu et le pied de page du site
require_once 'src/presentation.php';

// Appel de la page qui gère les affichages de la page accueil
require_once 'src/controller/controller_accueil.php';


// Appel de la page qui gère les affichages de la page login
require_once 'src/controller/controller_login.php';
require_once 'src/controller/controller_mentionslegales.php';
require_once 'src/controller/controller_apropos.php';



require_once 'src/controller/controller_admin.php';
require_once 'src/controller/controller_prof.php';
require_once 'src/controller/controller_eleves.php';

require_once 'src/controller/controller_reseau.php';
require_once 'src/controller/controller_documents.php';

require_once 'src/controller/controller_filtreacces.php';
require_once 'src/controller/controller_connexion.php';

require_once 'src/controller/controller_classe.php';



require_once 'classes/class_eleve.php';
require_once 'classes/class_professeur.php';
require_once 'classes/class_classe.php';
require_once 'classes/class_admin.php';
require_once 'classes/class_classeprofesseur.php';
require_once 'classes/class_classeeleve.php';
require_once 'classes/class_connexion.php';

require_once 'classes/class_accueil.php';



// Appel la page qui contient l'ensemble des paramètre pour ce connecter à la base de donnée
require_once 'src/config.php';

// Appel de la base de donnée
require_once 'src/bd/connexion.php';

// Appel de la d'une fonction qui génére aléatoirement un mot de passe
require_once 'src/mdp.php';



$db = connect($config); // Connexion à la base de données
$contenu = getPage();


//On affiche l'entête du site
entete();
   
// Si la base de données renvoie une valeur différente de NULL
if ($db != NULL){

 	if ($contenu != "Login" && $contenu != "Passwd"){
	    menu($db) ;
	     // On affiche la page normalement
	    // Demande le nom de la fonction qui gère l'affichage de la page souhaitée

	    // Exécution de la fonction souhaitée
	    $contenu($db);
	    
	    // On affiche le bas de la page
	    bas();
	}

	else{
		// On affiche seulement le contenu
		$contenu($db);
	}
}


// Sinon nous indiquons à l'utilisateur que le site est indisponible
else { 
    echo "<style>h1{
    margin-top: 45vh;
    margin-left: 40%;
    display : block;
    }
</style>";
    echo "<h1>Site indisponible</h1>";
}

?> 