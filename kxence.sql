-- phpMyAdmin SQL Dump
-- version 4.2.12deb2
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mar 23 Janvier 2018 à 20:34
-- Version du serveur :  5.5.44-0+deb8u1
-- Version de PHP :  5.6.7-1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `kxence`
--

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `pseudo` varchar(100) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `nom` varchar(32) NOT NULL,
  `prenom` varchar(32) NOT NULL,
  `mdp` varchar(300) NOT NULL,
  `photo` varchar(200) NOT NULL,
  `anniversaire` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `admin`
--

INSERT INTO `admin` (`pseudo`, `mail`, `nom`, `prenom`, `mdp`, `photo`, `anniversaire`) VALUES
('aLBorniche17', 'borniche.leo@gmail.com', 'Borniche', 'Léo', '0fc2222182dab1cdd06544da6e36a342d0df6025', 'IMG_0168.jpg', '1998-10-09'),
('aLChevalier17', 'louison.chevalier@gmail.com', 'Chevalier', 'Louison', '963d46537997600693d50a31393d36979abddd34', '24_128.png', '1998-09-04'),
('aVGorlier17', 'valentin.gorlier@hotmail.fr', 'Gorlier', 'Valentin', '963d46537997600693d50a31393d36979abddd34', '24_128.png', '1998-10-16');

-- --------------------------------------------------------

--
-- Structure de la table `classe`
--

CREATE TABLE IF NOT EXISTS `classe` (
`numClasse` int(11) NOT NULL,
  `nomClasse` varchar(100) NOT NULL,
  `anneeDeb` year(4) NOT NULL,
  `anneeFin` year(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `classe`
--

INSERT INTO `classe` (`numClasse`, `nomClasse`, `anneeDeb`, `anneeFin`) VALUES
(3, 'BTS SIO - SLAM', 2017, 2018),
(4, 'BTS SIO - SISR', 2017, 2018),
(5, 'BTS SIO - 1A', 2016, 2017),
(6, 'Classe test', 2015, 2016);

-- --------------------------------------------------------

--
-- Structure de la table `classeeleve`
--

CREATE TABLE IF NOT EXISTS `classeeleve` (
`id` int(11) NOT NULL,
  `pseudo` varchar(100) CHARACTER SET utf8 NOT NULL,
  `numClasse` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `classeeleve`
--

INSERT INTO `classeeleve` (`id`, `pseudo`, `numClasse`) VALUES
(1, 'eLChevalier17', 3),
(2, 'eVGorlier17', 3),
(3, 'eFFaidherbe17', 4),
(4, 'eJBebek17', 4),
(5, 'eKLaurito17', 4),
(6, 'eMRoger17', 4),
(19, 'eFFaidherbe17', 5),
(20, 'eJBebek17', 5),
(21, 'eKLaurito17', 5),
(22, 'eLChevalier17', 5),
(23, 'eMRoger17', 5),
(24, 'eVGorlier17', 5),
(31, 'eFFaidherbe17', 6),
(32, 'eJBebek17', 6),
(33, 'eLChevalier17', 6),
(34, 'eMRoger17', 6),
(35, 'eVGorlier17', 6);

-- --------------------------------------------------------

--
-- Structure de la table `classeprofesseur`
--

CREATE TABLE IF NOT EXISTS `classeprofesseur` (
`id` int(11) NOT NULL,
  `pseudo` varchar(100) CHARACTER SET utf8 NOT NULL,
  `numClasse` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `classeprofesseur`
--

INSERT INTO `classeprofesseur` (`id`, `pseudo`, `numClasse`) VALUES
(1, 'pJLe Gales17', 3),
(2, 'pMPouchain17', 3),
(3, 'pRMonfilliette17', 3),
(4, 'pALecomte17', 4),
(5, 'pCDudek17', 4),
(16, 'pCDudek17', 5),
(17, 'pFBallesta17', 5),
(18, 'pJLe Gales17', 5),
(19, 'pMPouchain17', 5),
(20, 'pRMonfilliette17', 5),
(23, 'pMPouchain17', 6),
(24, 'pALecomte17', 6),
(25, 'pJLe Gales17', 6);

-- --------------------------------------------------------

--
-- Structure de la table `connexion`
--

CREATE TABLE IF NOT EXISTS `connexion` (
`id` int(11) NOT NULL,
  `pseudo` varchar(300) CHARACTER SET utf8 NOT NULL,
  `role` varchar(30) CHARACTER SET utf8 NOT NULL,
  `remoteip` varchar(200) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `navigateur` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `connexion`
--

INSERT INTO `connexion` (`id`, `pseudo`, `role`, `remoteip`, `date`, `navigateur`) VALUES
(39, 'aLChevalier17', 'admins', '192.168.43.173', '2017-12-11 08:41:59', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:57.0) Gecko/20100101 Firefox/57.0'),
(40, 'aVGorlier17', 'admins', '192.168.43.43', '2017-12-11 08:45:05', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36'),
(41, 'aLChevalier17', 'admins', '192.168.43.173', '2017-12-11 10:16:21', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:57.0) Gecko/20100101 Firefox/57.0'),
(42, 'aVGorlier17', 'admins', '192.168.43.43', '2017-12-11 10:18:41', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36'),
(43, 'aVGorlier17', 'admins', '192.168.43.43', '2017-12-11 10:20:23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36'),
(44, 'aVGorlier17', 'admins', '192.168.43.43', '2017-12-11 10:23:25', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36'),
(45, 'aVGorlier17', 'admins', '192.168.43.43', '2017-12-11 10:24:04', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36'),
(46, 'aVGorlier17', 'admins', '192.168.43.43', '2017-12-11 10:27:16', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36'),
(47, 'aLChevalier17', 'admins', '192.168.43.173', '2017-12-11 10:32:08', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:57.0) Gecko/20100101 Firefox/57.0'),
(48, 'aVGorlier17', 'admins', '192.168.43.43', '2017-12-11 12:19:36', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36'),
(49, 'aLChevalier17', 'admins', '192.168.43.173', '2017-12-11 12:33:57', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/604.4.7 (KHTML, like Gecko) Version/11.0.2 Safari/604.4.7'),
(50, 'aLChevalier17', 'admins', '192.168.43.173', '2017-12-11 13:13:58', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:57.0) Gecko/20100101 Firefox/57.0'),
(51, 'aVGorlier17', 'admins', '192.168.43.43', '2017-12-11 13:24:38', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 Edge/16.16299'),
(52, 'aVGorlier17', 'admins', '192.168.43.43', '2017-12-11 13:25:57', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36'),
(53, 'aVGorlier17', 'admins', '192.168.43.43', '2017-12-11 13:28:00', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36'),
(54, 'aVGorlier17', 'admins', '192.168.43.43', '2017-12-11 13:28:34', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 Edge/16.16299'),
(55, 'aLChevalier17', 'admins', '192.168.43.173', '2017-12-11 13:40:40', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/604.4.7 (KHTML, like Gecko) Version/11.0.2 Safari/604.4.7'),
(56, 'aVGorlier17', 'admins', '192.168.43.43', '2017-12-11 14:11:17', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36'),
(57, 'aVGorlier17', 'admins', '192.168.43.43', '2017-12-11 14:27:10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 Edge/16.16299'),
(58, 'aLChevalier17', 'admins', '192.168.1.55', '2017-12-11 18:38:16', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:57.0) Gecko/20100101 Firefox/57.0'),
(59, 'aLChevalier17', 'admins', '192.168.1.55', '2017-12-11 20:10:27', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:57.0) Gecko/20100101 Firefox/57.0'),
(60, 'aLChevalier17', 'admins', '192.168.1.55', '2017-12-12 19:21:37', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:57.0) Gecko/20100101 Firefox/57.0'),
(61, 'aLChevalier17', 'admins', '192.168.1.55', '2017-12-12 19:26:01', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:57.0) Gecko/20100101 Firefox/57.0'),
(62, 'aLChevalier17', 'admins', '192.168.1.55', '2017-12-12 19:26:50', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:57.0) Gecko/20100101 Firefox/57.0'),
(63, 'aVGorlier17', 'admins', '192.168.43.173', '2017-12-19 15:17:01', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/604.4.7 (KHTML, like Gecko) Version/11.0.2 Safari/604.4.7');

-- --------------------------------------------------------

--
-- Structure de la table `eleve`
--

CREATE TABLE IF NOT EXISTS `eleve` (
  `pseudo` varchar(100) NOT NULL,
  `mail` varchar(100) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `prenom` varchar(100) NOT NULL,
  `mdp` varchar(300) NOT NULL,
  `photo` varchar(150) NOT NULL,
  `anniversaire` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `eleve`
--

INSERT INTO `eleve` (`pseudo`, `mail`, `nom`, `prenom`, `mdp`, `photo`, `anniversaire`) VALUES
('eFFaidherbe17', 'franck.faidherbe@hotmail.fr', 'Faidherbe', 'Franck', 'b36cbab862c26e652c41fdca533461edcc3fe9a3', '2017_04_12_19.07.26.jpg', '1996-11-04'),
('eJBebek17', 'jojobebek@hotmail.fr', 'Bebek', 'Jordan', 'ae33b6d2bb3dc7430f86066301786ffbdf6c69db', 'image.jpg', '1997-05-01'),
('eKLaurito17', 'kevin.laurito@hotmail.fr', 'Laurito', 'Kevin', 'da39a3ee5e6b4b0d3255bfef95601890afd80709', '2017_04_24_01.47.16_crokjjhp.jpg', '1996-10-04'),
('eLChevalier17', 'louison.chevalier@gmail.com', 'Chevalier', 'Louison', '963d46537997600693d50a31393d36979abddd34', '20604311_10207467534725076_5536630187468301758_n.jpg', '1998-09-04'),
('eMRoger17', 'maxence.roger@hotmail.fr', 'Roger', 'Maxence', 'c57d6cac52c47e4b0f7cf884d489bd37570650f5', 'JPEG_20170911_jj122729.jpg', '1998-05-09'),
('eVGorlier17', 'valentin.gorlier@hotmail.fr', 'Gorlier', 'Valentin', '963d46537997600693d50a31393d36979abddd34', 'Snapchat_1998849884.jpg', '1998-10-16');

-- --------------------------------------------------------

--
-- Structure de la table `professeur`
--

CREATE TABLE IF NOT EXISTS `professeur` (
  `pseudo` varchar(100) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `nom` varchar(32) NOT NULL,
  `prenom` varchar(32) NOT NULL,
  `mdp` varchar(300) NOT NULL,
  `photo` varchar(150) NOT NULL,
  `anniversaire` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `professeur`
--

INSERT INTO `professeur` (`pseudo`, `mail`, `nom`, `prenom`, `mdp`, `photo`, `anniversaire`) VALUES
('pALecomte17', 'augustin.lecomte@gmail.com', 'Lecomte', 'Augustin', '40149ac8b53f4dcb19473ce76a5975bdebc131af', '14184480_174591682971935_6225527732578235414_n.jpg', '1990-12-18'),
('pCDudek17', 'dudek.cedric@gmail.com', 'Dudek', 'Cédric', '30f7fcffa7f6c2ad89269da557f2a518f8c9e361', 'ghost_person_100x100_v1.png', '1980-11-10'),
('pFBallesta17', 'florian.ballesta@gmail.com', 'Ballesta', 'Florian', 'f42eab4749efdce896223c04fb18053a9b4cc566', '13495152_10153968407084221_5410791345275067045_n.jpg', '1988-12-19'),
('pJLe Gales17', 'julien.legales@gmail.com', 'Le Gales', 'Julien', '963d46537997600693d50a31393d36979abddd34', '21767913_230280067501367_880056722674567111_n.jpg', '1980-11-01'),
('pMPouchain17', 'pouchain.marieange@orange.fr', 'Pouchain', 'Marie-Ange', 'da39a3ee5e6b4b0d3255bfef95601890afd80709', 'pouch1.jpg', '1964-11-01'),
('pRMonfilliette17', 'regis.monfilliette@gmail.com', 'Monfilliette', 'Régis', 'fd2038015d1c3dab278f9dab7ce8122cb26e4a25', 'DSC00865.jpg', '1917-11-27');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `admin`
--
ALTER TABLE `admin`
 ADD PRIMARY KEY (`pseudo`);

--
-- Index pour la table `classe`
--
ALTER TABLE `classe`
 ADD PRIMARY KEY (`numClasse`);

--
-- Index pour la table `classeeleve`
--
ALTER TABLE `classeeleve`
 ADD PRIMARY KEY (`id`), ADD KEY `pseudo` (`pseudo`), ADD KEY `numClasse` (`numClasse`);

--
-- Index pour la table `classeprofesseur`
--
ALTER TABLE `classeprofesseur`
 ADD PRIMARY KEY (`id`), ADD KEY `pseudo` (`pseudo`), ADD KEY `numClasse` (`numClasse`);

--
-- Index pour la table `connexion`
--
ALTER TABLE `connexion`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `eleve`
--
ALTER TABLE `eleve`
 ADD PRIMARY KEY (`pseudo`);

--
-- Index pour la table `professeur`
--
ALTER TABLE `professeur`
 ADD PRIMARY KEY (`pseudo`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `classe`
--
ALTER TABLE `classe`
MODIFY `numClasse` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `classeeleve`
--
ALTER TABLE `classeeleve`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT pour la table `classeprofesseur`
--
ALTER TABLE `classeprofesseur`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT pour la table `connexion`
--
ALTER TABLE `connexion`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=64;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `classeeleve`
--
ALTER TABLE `classeeleve`
ADD CONSTRAINT `classeeleve_ibfk_1` FOREIGN KEY (`numClasse`) REFERENCES `classe` (`numClasse`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `classeeleve_ibfk_2` FOREIGN KEY (`pseudo`) REFERENCES `eleve` (`pseudo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `classeprofesseur`
--
ALTER TABLE `classeprofesseur`
ADD CONSTRAINT `classeprofesseur_ibfk_1` FOREIGN KEY (`numClasse`) REFERENCES `classe` (`numClasse`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `classeprofesseur_ibfk_2` FOREIGN KEY (`pseudo`) REFERENCES `professeur` (`pseudo`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
