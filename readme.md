# Kxence projet

Kxence est une application mise à disposition pour les école primaire.

En bref c'est :

- Un accès à internet contrôlé
- Un OS léger, siple et gratuit
- Une création de compte rapide
- Sauvegarde de documents automatique
- Restauration de machines


[![Vidéo de présentation](https://i.ytimg.com/vi/U8yCPUTtJN8/hqdefault.jpg?sqp=-oaymwEZCNACELwBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLAo4aiVmH4V9MJfFYb34YhWuhONwg.jpeg)](https://www.youtube.com/watch?v=U8yCPUTtJN8)
