<?php

 	
 function Accueil($db){
 echo '
        <div class="content-inner">
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Accueil</h2>
            </div>
          </header>
          
          <section class="dashboard-counts no-padding-bottom">
            <div class="container-fluid">
              <div class="row bg-white has-shadow">
              ';
 
            //CALCUL ADMIN
            $sql = "SELECT count(pseudo) from admin";
            $req2 = $db->query ($sql);
            $red3 = $req2->fetch();
            $totaladmin = ($red3[0]);

               
            //CALCUL PROF
            $sql1 = "SELECT count(pseudo) from professeur";
            $req4 = $db->query ($sql1);
            $red5 = $req4->fetch();
            $totalprof = ($red5[0]);

            //CALCUL ELEVE
            $sql2 = "SELECT count(pseudo) from eleve";
            $req6 = $db->query ($sql2);
            $red7 = $req6->fetch();
            $totaleleve = ($red7[0]);
            
            //TOTAL (ADMIN-PROF-ELEVE)
            $total = ($totaladmin+$totalprof+$totaleleve);


            //POURCENTAGE ADMIN
            $pourcentageadmin = ($totaladmin/$total)*100;


            //POURCENTAGE PROF
            $pourcentageprof = ($totalprof/$total)*100;

            //POURCENTAGE PROF
            $pourcentageeleve = ($totaleleve/$total)*100;

    
            
                echo'
                <div class="col-xl-3 col-sm-6">
                  <div class="item d-flex align-items-center">
                    <div class="icon bg-violet"><i class="fa fa-user-secret"></i></div>
                    <div class="title"><span>Admins</span>
                      <div class="progress">
                        <div role="progressbar" style="width:'.$pourcentageadmin.'%; height: 4px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-violet"></div>
                      </div>
                    </div>
                    <div class="number"><strong>'; echo $totaladmin; echo'</strong></div>
                  </div>
                </div>



                <div class="col-xl-3 col-sm-6">
                  <div class="item d-flex align-items-center">
                    <div class="icon bg-red"><i class="fa fa-user"></i></div>
                    <div class="title"><span>Profs</span>
                      <div class="progress">
                        <div role="progressbar" style="width:'.$pourcentageprof.'%; height: 4px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-red"></div>
                      </div>
                    </div>
                    <div class="number"><strong>'; echo $totalprof; echo'</strong></div>
                  </div>
                </div>
                <!-- Item -->
                <div class="col-xl-3 col-sm-6">
                  <div class="item d-flex align-items-center">
                    <div class="icon bg-green"><i class="fa fa-user"></i></div>
                    <div class="title"><span>Élèves</span>
                      <div class="progress">
                        <div role="progressbar" style="width:'.$pourcentageeleve.'%; height: 4px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-green"></div>
                      </div>
                    </div>
                    <div class="number"><strong>'; echo $totaleleve; echo'</strong></div>
                  </div>
                </div>
                <!-- Item -->
                <div class="col-xl-3 col-sm-6">
                  <div class="item d-flex align-items-center">
                    <div class="icon bg-orange"><i class="fa fa-group"></i></div>
                    <div class="title"><span>Total</span>
                      <div class="progress">
                        <div role="progressbar" style="width:100%; height: 4px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-orange"></div>
                      </div>
                    </div>
                    <div class="number"><strong>'; echo $total; echo'</strong></div>
                  </div>
                </div>
              </div>
            </div>
          </section>';
                    echo'
          <!-- Dashboard Header Section    -->
          <section class="dashboard-header">
            <div class="container-fluid">
              <div class="row">
               
                <!-- Line Chart            -->
                <div class="chart col-lg-6 col-12">
                  <div class="line-chart bg-white d-flex align-items-center justify-content-center has-shadow">
                    <canvas id="lineCahrt"></canvas>
                  </div>
                </div>
                <div class="chart col-lg-6 col-12" style="text-align: center;">
                  <div class="bar-chart has-shadow bg-white">
                    <div style="height:20px;"></div>
                        <select class="form-control">
                            <option  id="ip" name="ip" value="9.9.9.9">9.9.9.9</option>
                            <option  id="ip" name="ip" value="8.8.8.8">8.8.8.8</option>
                          </select>
                        <div style="padding: 50px;  id="panneau" >	
                           <p style="text-align: center;"><input input type="submit" id="btping" name="btping" value="Ping" class="mx-sm-3 btn btn-primary"></p>
                           
                        </div>
                        
                        <div id="valeurip"></div>
                        <div " id="ping"></div>
                  </div>
                  
                </div>
              </div>
            </div>
          </section>';
          
            
            $professeur = new professeur($db);
            $listeProfesseur = $professeur->selectAnniversaire();
            $nb = count ($listeProfesseur);
            
            
            if ($nb != NULL){
                
                
                if ($nb > 1){
                    $titre = "Anniversaires";
                }
                else{
                    $titre = "Anniversaire";
                    
                }
                
                echo' 
          <section class="feeds no-padding-top">
            <div class="container-fluid">
              <div class="row">
                <div class="col-lg-4">
                  <div class="articles card">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                        <div aria-labelledby="closeCard" class="dropdown-menu has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a></div>
                      </div>
                    </div>
                    <div class="card-header d-flex align-items-center">
                      <h2 class="h3">'.$titre.'</h2>
                    </div>
                    
                    <div class="card-body no-padding">';
                     foreach($listeProfesseur as $Professeur){
                         
                        $nom = $Professeur['nom'];
                        $prenom = $Professeur['prenom'];
                        $anniversaire = $Professeur['anniversaire'];
                        $photo = $Professeur['photo'];
                        $dateactuelle = date('Y-m-d');
                        $age = $dateactuelle - $anniversaire;
                        
                         echo'<div class="item d-flex align-items-center">
                        <div class="image"><img src="assets/profils/professeurs/'.$photo.'" class="img-fluid rounded-circle"></div>
                        <div class="text"><a href="#">
                            <h3 class="h5">'.$nom.' '.$prenom.'</h3></a><small>'.$age.' ans !</small></div>
                      </div>';
                    }
             
                    echo' 
                      
                    </div>
                  </div>
                </div>';
            }
            
                

            echo'
                <!-- Check List -->
                <div class="col-lg-4">
                  <div class="checklist card">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                        <div aria-labelledby="closeCard" class="dropdown-menu has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
                      </div>
                    </div>
                    <div class="card-header d-flex align-items-center">           
                      <h2 class="h3">To Do List </h2>
                    </div>
                    <div class="card-body no-padding">
                      <div class="item d-flex">
                        <input type="checkbox" id="input-1" name="input-1" class="checkbox-template">
                        <label for="input-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</label>
                      </div>
                      <div class="item d-flex">
                        <input type="checkbox" id="input-2" name="input-2" class="checkbox-template">
                        <label for="input-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</label>
                      </div>
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
         



          <!-- Updates Section                                                -->
          <section class="updates no-padding-top">
            <div class="container-fluid">
              <div class="row">
                <!-- Recent Updates-->
                <div class="col-lg-4">
                  <div class="recent-updates card">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                        <div aria-labelledby="closeCard" class="dropdown-menu has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
                      </div>
                    </div>
                    <div class="card-header">
                      <h3 class="h4">Recent Updates</h3>
                    </div>
                    <div class="card-body no-padding">
                      <!-- Item-->
                      <div class="item d-flex justify-content-between">
                        <div class="info d-flex">
                          <div class="icon"><i class="icon-rss-feed"></i></div>
                          <div class="title">
                            <h5>Lorem ipsum dolor sit amet.</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed.</p>
                          </div>
                        </div>
                        <div class="date text-right"><strong>24</strong><span>May</span></div>
                      </div>
                      <!-- Item-->
                      <div class="item d-flex justify-content-between">
                        <div class="info d-flex">
                          <div class="icon"><i class="icon-rss-feed"></i></div>
                          <div class="title">
                            <h5>Lorem ipsum dolor sit amet.</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed.</p>
                          </div>
                        </div>
                        <div class="date text-right"><strong>24</strong><span>May</span></div>
                      </div>
                      <!-- Item        -->
                      <div class="item d-flex justify-content-between">
                        <div class="info d-flex">
                          <div class="icon"><i class="icon-rss-feed"></i></div>
                          <div class="title">
                            <h5>Lorem ipsum dolor sit amet.</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed.</p>
                          </div>
                        </div>
                        <div class="date text-right"><strong>24</strong><span>May</span></div>
                      </div>
                      <!-- Item-->
                      <div class="item d-flex justify-content-between">
                        <div class="info d-flex">
                          <div class="icon"><i class="icon-rss-feed"></i></div>
                          <div class="title">
                            <h5>Lorem ipsum dolor sit amet.</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed.</p>
                          </div>
                        </div>
                        <div class="date text-right"><strong>24</strong><span>May</span></div>
                      </div>
                      <!-- Item-->
                      <div class="item d-flex justify-content-between">
                        <div class="info d-flex">
                          <div class="icon"><i class="icon-rss-feed"></i></div>
                          <div class="title">
                            <h5>Lorem ipsum dolor sit amet.</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed.</p>
                          </div>
                        </div>
                        <div class="date text-right"><strong>24</strong><span>May</span></div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Daily Feeds -->
                <div class="col-lg-4">
                  <div class="daily-feeds card"> 
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                        <div aria-labelledby="closeCard" class="dropdown-menu has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
                      </div>
                    </div>
                    <div class="card-header">
                      <h3 class="h4">Daily Feeds</h3>
                    </div>
                    <div class="card-body no-padding">
                      <!-- Item-->
                      <div class="item">
                        <div class="feed d-flex justify-content-between">
                          <div class="feed-body d-flex justify-content-between"><a href="#" class="feed-profile"><img src="img/avatar-5.jpg" alt="person" class="img-fluid rounded-circle"></a>
                            <div class="content">
                              <h5>Aria Smith</h5><span>Posted a new blog </span>
                              <div class="full-date"><small>Today 5:60 pm - 12.06.2014</small></div>
                            </div>
                          </div>
                          <div class="date text-right"><small>5min ago</small></div>
                        </div>
                      </div>
                      <!-- Item-->
                      <div class="item"> 
                        <div class="feed d-flex justify-content-between">
                          <div class="feed-body d-flex justify-content-between"><a href="#" class="feed-profile"><img src="img/avatar-2.jpg" alt="person" class="img-fluid rounded-circle"></a>
                            <div class="content">
                              <h5>Frank Williams</h5><span>Posted a new blog </span>
                              <div class="full-date"><small>Today 5:60 pm - 12.06.2014</small></div>
                              <div class="CTAs"><a href="#" class="btn btn-xs btn-secondary"><i class="fa fa-thumbs-up"> </i>Like</a><a href="#" class="btn btn-xs btn-secondary"><i class="fa fa-heart"> </i>Love    </a></div>
                            </div>
                          </div>
                          <div class="date text-right"><small>5min ago</small></div>
                        </div>
                      </div>
                      <!-- Item-->
                      <div class="item clearfix">
                        <div class="feed d-flex justify-content-between">
                          <div class="feed-body d-flex justify-content-between"><a href="#" class="feed-profile"><img src="img/avatar-3.jpg" alt="person" class="img-fluid rounded-circle"></a>
                            <div class="content">
                              <h5>Ashley Wood</h5><span>Posted a new blog </span>
                              <div class="full-date"><small>Today 5:60 pm - 12.06.2014</small></div>
                            </div>
                          </div>
                          <div class="date text-right"><small>5min ago</small></div>
                        </div>
                        <div class="quote has-shadow"> <small>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s. Over the years.</small></div>
                        <div class="CTAs pull-right"><a href="#" class="btn btn-xs btn-secondary"><i class="fa fa-thumbs-up"> </i>Like</a></div>
                      </div>
                    </div>
                  </div>
                </div>
                

                <!-- Recent Activities -->
                <div class="col-lg-4">
                  <div class="recent-activities card">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                        <div aria-labelledby="closeCard" class="dropdown-menu has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
                      </div>
                    </div>
                    <div class="card-header">
                      <h3 class="h4">Recent Activities</h3>
                    </div>
                    <div class="card-body no-padding">
                      <div class="item">
                        <div class="row">
                          <div class="col-4 date-holder text-right">
                            <div class="icon"><i class="icon-clock"></i></div>
                            <div class="date"> <span>6:00 am</span><span class="text-info">6 hours ago</span></div>
                          </div>
                          <div class="col-8 content">
                            <h5>Meeting</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
                          </div>
                        </div>
                      </div>
                      <div class="item">
                        <div class="row">
                          <div class="col-4 date-holder text-right">
                            <div class="icon"><i class="icon-clock"></i></div>
                            <div class="date"> <span>6:00 am</span><span class="text-info">6 hours ago</span></div>
                          </div>
                          <div class="col-8 content">
                            <h5>Meeting</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
                          </div>
                        </div>
                      </div>
                      <div class="item">
                        <div class="row">
                          <div class="col-4 date-holder text-right">
                            <div class="icon"><i class="icon-clock"></i></div>
                            <div class="date"> <span>6:00 am</span><span class="text-info">6 hours ago</span></div>
                          </div>
                          <div class="col-8 content">
                            <h5>Meeting</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

';
}


function after(){
	echo'
	<!-- Page Footer-->
          <footer class="main-footer">
            <div class="container-fluid">
              <div class="row">
                <div class="col-sm-6">
                  <p>GORLIER Valentin - CHEVALIER Louison &copy; 2017-2018</p>
                </div>
                <div class="col-sm-6 text-right">
                  <p>KXENCE</a></p>
                </div>
              </div>
            </div>
          </footer>
        </div>
      </div>

	';
}





?>
