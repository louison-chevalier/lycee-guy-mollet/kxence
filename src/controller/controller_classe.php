<?php

function classe($db){

echo '
    <div class="content-inner">
        <header class="page-header">
            <div class="container-fluid">
                <h2 class="no-margin-bottom">Gestion des Classes</h2>
            </div>
        </header>
        
        
        ';
        
        $classe = new classe($db);
        $listeannee = $classe->listeAnnee();
    
        
    echo'<div class="col-lg-12" style="margin-top: 35px;">                           
                <div class="card" >
        <nav>
            <ul class="pagination">
                <li class="page-item"><a class="page-link" href="index.php?page=classe">Gestion</a></li>';
          
                foreach($listeannee as $uneAnnee){
                    echo'<li class="page-item"><a class="page-link" href="index.php?page=classe&annesscolaire='.utf8_encode($uneAnnee['anneeDeb'] ).'">'.utf8_encode($uneAnnee['anneeDeb'] ).'</a></li>';
                }
    echo'   </ul>
        </nav></div></div>';
    
    
    
    if(isset($_GET['annesscolaire'])){
        $anneeDeb = $_GET['annesscolaire'];
 
        
        $listeClasse = $classe->selectAnnee($anneeDeb);
        foreach($listeClasse as $uneClasse){

            echo'  
              
                    <div class="col-lg-12">
                      <div class="card">
                        <div class="card-close">
                          <div class="dropdown">
                            <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                            <div aria-labelledby="closeCard" class="dropdown-menu has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a>
                            </div>
                          </div>
                        </div>
                        <div class="card-header d-flex align-items-center">
                          <h3 class="h4">'.($uneClasse['nomClasse'] ).'</h3>
                        </div>
                        <div class="card-body">
                          <table class="table  table-hover">
                            <thead>
                              <tr>
                                <th>Photo</th>
                                <th>Pseudo</th>
                                <th>Nom</th>
                                <th>Prénom</th>
                                <th>Mail</th>
                              </tr>
                            </thead>
                            <tbody>';

                            $numClasse =  $uneClasse['numClasse'];
                            $classeprofesseur = new classeprofesseur($db);
                            $listeclasseprofesseur = $classeprofesseur->selectProf($numClasse);


                            foreach($listeclasseprofesseur as $unProf){
                              echo'
                              <tr style="background-color : #fffcfc;">
                                <td><img height="70px;" src="assets/profils/professeurs/'.utf8_encode($unProf['photo'] ).'"></td>
                                <td>'.$unProf['pseudo'].'</td>
                                <td>'.$unProf['nom'].'</td>
                                <td>'.$unProf['prenom'].'</td>
                                <td>'.$unProf['mail'].'</td>
                              </tr>';
                            }

                            $classeeleve = new classeeleve($db);
                            $listeclasseeleve = $classeeleve->selectEleve($numClasse);

                            foreach($listeclasseeleve as $unEleve){
                              echo'
                              <tr>
                                <td><img height="70px;" src="assets/profils/eleves/'.utf8_encode($unEleve['photo'] ).'"></td>
                                <td>'.$unEleve['pseudo'].'</td>
                                <td>'.$unEleve['nom'].'</td>
                                <td>'.$unEleve['prenom'].'</td>
                                <td>'.$unEleve['mail'].'</td>
                              </tr>';
                            }

                            echo'  
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  ';
        }
    }
    
    else{
    
    //Lorsque on a appuyer sur le bouton modifier
    if(isset($_POST['btModifier'])){

      //On récupere ce qui est cocher
      if (isset($_POST['cocher'])){
        $liste = $_POST['cocher'];
        if($liste != NULL){
          $classe = new classe($db);
          //Pour le premier pseudo de la liste
          foreach($liste as $numClasse){
            $classe = $classe->selectOne($numClasse);
            $numClasse = $classe['numClasse'];
            $nomClasse = $classe['nomClasse'];
            $anneeDeb = $classe['anneeDeb'];
            $anneeFin = $classe['anneeFin'];
            
            $professeur = new professeur($db);
            $listeprof = $professeur->selectAll();

            $eleve = new eleve($db);
            $listeeleve = $eleve->selectAll();
            
            echo'
              <div class="col-lg-12">                           
                <div class="card">

                  <div class="card-close">
                    <div class="dropdown">
                      <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                        <i class="fa fa-ellipsis-v"></i>
                      </button>
                      <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
                        <a href="#" class="dropdown-item remove"><i class="fa fa-times"></i>Close</a>
                      </div>
                    </div>
                  </div>

                  <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Modifier la classe : '.$nomClasse .'</h3>
                  </div>

                  <form class="login-container" method="post" action= "index.php?page=classe" enctype="multipart/form-data">
                  <div class="row" style="margin-top: 18px; padding: 10px;">
                  <input type="hidden" name="numClasse"  value="'.$numClasse.'" />
                  
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nom</label>
                      <input required="" type="text" class="form-control" id="exampleInputEmail1" value="'.$nomClasse.'" name="nomClasse" aria-describedby="emailHelp"  >
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Année début</label>
                        <input aria-describedby="emailHelp"  type="number" step="1" value="'.$anneeDeb.'" min="2000" max="2100" class="form-control" id="exampleInputEmail1" name="anneeDeb">
                    </div>
                  </div>
                  
                 
                   <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Année fin</label>
                        <input aria-describedby="emailHelp"  type="number" step="1" value="'.$anneeFin.'" min="2000" max="2100" class="form-control" id="exampleInputEmail1" name="anneeFin">
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                    
                    <!-- http://loudev.com/ -->
                    <select id="pre-selected-options"  name="listeclasse[]" multiple="multiple">
                    
                  <optgroup label="Professeurs">';
                  
                        $classeprofesseur = new classeprofesseur($db);
                        $listeclassenoprofesseur = $classeprofesseur->selectnoProf($numClasse);


                        foreach($listeclassenoprofesseur as $unProf){
                              echo'<option style="text-transform: uppercase;" value="professeur|'.utf8_encode($unProf['pseudo'] ).'">'.utf8_encode($unProf['nom'] ).' '.utf8_encode($unProf['prenom'] ).'</option>';
                          }


                        $classeprofesseur = new classeprofesseur($db);
                        $listeclasseprofesseur = $classeprofesseur->selectProf($numClasse);


                          foreach($listeclasseprofesseur as $unProf){
                              echo'<option selected style="text-transform: uppercase;" value="professeur|'.utf8_encode($unProf['pseudo'] ).'" >'.utf8_encode($unProf['nom'] ).' '.utf8_encode($unProf['prenom'] ).'</option>';
                          }
                          echo' 
                  </optgroup>
                  
                  
                  <optgroup label="Élèves">';
                        $classeeleve = new classeeleve($db);
                        $listeclassenoeleve = $classeeleve->selectnoEleve($numClasse);     

                        foreach($listeclassenoeleve as $unEleve){
                            echo'<option style="text-transform: uppercase;" value="eleve|'.utf8_encode($unEleve['pseudo'] ).'">'.utf8_encode($unEleve['nom'] ).' '.utf8_encode($unEleve['prenom'] ).'</option>';
                        }

                        $classeeleve = new classeeleve($db);
                        $listeclasseeleve = $classeeleve->selectEleve($numClasse);


                        foreach($listeclasseeleve as $unEleve){
                            echo'<option selected style="text-transform: uppercase;" value="eleve|'.utf8_encode($unEleve['pseudo'] ).'">'.utf8_encode($unEleve['nom'] ).' '.utf8_encode($unEleve['prenom'] ).'</option>';
                        }
                        echo' 
                  </optgroup>
                  
                </select>
                 </div>
                  </div>
                <div class="form-group">
                  <p style="text-align: center;"><input input type="submit" id="btValider" name="btValider" value="Modifier" class="mx-sm-3 btn btn-primary"></p>
                </div>
            </form>
            
                </div>
              </div>
              ';
          }
        }
      }
      else{
         echo "<script type='text/javascript'>document.location.replace('index.php?page=administrateur&selec');</script>";
      } 
    }
    
    
    else{
        //Afficher une alerte lorsque je modifie une classe
        if(isset($_POST['btValider'])){
            
            $numClasse = $_POST['numClasse'];
            $classe = new classe($db);
            $Classe = $classe->selectOne($numClasse);
            
            $nomClasse1 = $Classe['nomClasse'];
            $anneeDeb1 = $Classe['anneeDeb'];
            $anneeFin1 = $Classe['anneeFin'];

            $nomClasse = $_POST['nomClasse'];
            $anneeDeb = $_POST['anneeDeb'];
            $anneeFin = $_POST['anneeFin'];

            if ($nomClasse == NULL){
              $nomClasse = $nomClasse1;
            }
            if ($anneeDeb == NULL){
              $anneeDeb = $anneeDeb1;
            }
            if ($anneeFin == NULL){
              $anneeFin = $anneeFin1;
            }
            
            $Classe = new classe($db);
            $nb2 = $Classe->deleteOne($numClasse);
            
            //On ajoute à la base de donnée
            $Classe = new classe($db);
            $nb1 = $classe->insert($numClasse, $nomClasse, $anneeDeb , $anneeFin);
            
            $listeclasse = $_POST['listeclasse'];
            $nbliste = count($listeclasse);
                
                $i =1;
                $eleveok = 0;
                $profok = 0;
                
                for ($i = 0; $i < $nbliste; $i++) {
                    list($r, $pseudo) = explode("|", $listeclasse[$i]);
                    $temp = 0;
                    if ($r == "eleve"){
                        $celeve = new classeeleve($db);
                        $temp = $celeve->insertAll($pseudo, $numClasse);
                        if ($temp=1){
                            $eleveok = $eleveok+1;
                        }
                    }
                    
                    $temp = 0;
                    if ($r == "professeur"){
                        $cprofesseur = new classeprofesseur($db);
                        $temp = $cprofesseur->insertAll($pseudo, $numClasse);
                        if ($temp=1){
                            $profok = $profok+1;
                        }
                    }
                    
                }
                
            $total = $nb1 + $nb2 + $nbliste;
            $nb = $nb1 + $nb2 + $profok + $eleveok;
            
            //Si il y a une erreur, on affiche une alerte     
            if ($nb!=$total){
                echo '
                    <div class="col-lg-12">                           
                        <div class="card">
                            <div class="card-close">
                              <div class="dropdown">
                                <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                                  <i class="fa fa-ellipsis-v"></i>
                                </button>
                                <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
                                  <a href="#" class="dropdown-item remove"><i class="fa fa-times"></i>Close</a>
                                </div>
                              </div>
                            </div>
                          <div  class="alert alert-danger" style="margin: 0px; padding: 18px; height: 60px; border-radius: 0px;" role="alert">Erreur d\'insertion</div>
                        </div>
                    </div>';
            }

            //Si il n'y a pas d'erreur, on affiche une alerte
            else{
                echo'
                    <div class="col-lg-12">                           
                        <div class="card">
                            <div class="card-close">
                              <div class="dropdown">
                                <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                                  <i class="fa fa-ellipsis-v"></i>
                                </button>
                                <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
                                  <a href="#" class="dropdown-item remove"><i class="fa fa-times"></i>Close</a>
                                </div>
                              </div>
                            </div>
                            <div class="alert alert-success" style="margin: 0px; padding: 18px; height: 60px; border-radius: 0px;" role="alert">Modification réussi</div>
                        </div>
                    </div>';
            }
        }
          
          
        if(isset($_POST['btAjouter'])){
            
            $classe = new classe($db);

            $nomClasse = $_POST['nomClasse'];
            $anneeDeb = $_POST['anneeDeb'];
            $anneeFin = $_POST['anneeFin'];
            
            //On ajoute à la base de donnée                 
            $nb = $classe->insertAll( $nomClasse, $anneeDeb , $anneeFin);
            
            $classe = new classe($db);
            $laClasse = $classe->selectNum($nomClasse, $anneeDeb , $anneeFin);
            $numClasse = $laClasse[0];
            
            
            $listeclasse = $_POST['listeclasse'];
            $nbliste = count($listeclasse);
                
                $i =1;
                for ($i = 0; $i < $nbliste; $i++) {
                    list($r, $pseudo) = explode("|", $listeclasse[$i]);
                    if ($r == "eleve"){
                        $celeve = new classeeleve($db);
                        $nb = $celeve->insertAll($pseudo, $numClasse); 
                    }
                    
                    if ($r == "professeur"){
                        $cprofesseur = new classeprofesseur($db);
                        $nb = $cprofesseur->insertAll($pseudo, $numClasse);   
                    }
                    
                }
                
            echo "<script type='text/javascript'>document.location.replace('index.php?page=classe&ajout=".$nb."');</script>";
            
        }
        
        if(isset($_GET['ajout'])){
            $nb = $_GET['ajout'];
            //Si il y a une erreur, on affiche une alerte     
            if ($nb!=1){
              echo '
                <div class="col-lg-12">                           
                  <div class="card">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                          <i class="fa fa-ellipsis-v"></i>
                        </button>
                        <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
                          <a href="#" class="dropdown-item remove"><i class="fa fa-times"></i>Close</a>
                        </div>
                      </div>
                    </div>
                    <div  class="alert alert-danger" style="margin: 0px; padding: 18px; height: 60px; border-radius: 0px;" role="alert">Erreur d\'insertion</div>
                  </div>
                </div>';
            }

            //Si il n'y a pas d'erreur, on affiche une alerte
            else{
              echo'
                <div class="col-lg-12">                           
                  <div class="card">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                          <i class="fa fa-ellipsis-v"></i>
                        </button>
                        <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
                          <a href="#" class="dropdown-item remove"><i class="fa fa-times"></i>Close</a>
                        </div>
                      </div>
                    </div>
                    <div class="alert alert-success" style="margin: 0px; padding: 18px; height: 60px; border-radius: 0px;" role="alert">Création de la classe réussi</div>
                  </div>
                </div>';
            }
        }
        
        //Lorsque on a appuyer sur le bouton supprimer
        if(isset($_POST['btSupprimer'])){
            //On récupere ce qui est cocher
            if (isset($_POST['cocher'])){
                $liste = $_POST['cocher'];

                $classe = new classe($db);
                //Pour le premier pseudo de la liste

                foreach($liste as $numClasse){
                   $uneClasse = $classe->selectOne($numClasse);
                    //on récupere le nom de la photo liée au pseudo
                    $nomdelaClasse = $uneClasse['nomClasse'];
                  //on supprime la classe
                  $nb = $classe->deleteOne($numClasse);
                }
                //On rafraichi la page, une nouvelle variable est placé en paramètre, elle récupére le pseudo de la personne supprimer
                //Pour ensuite afficher une alerte
                echo "<script type='text/javascript'>document.location.replace('index.php?page=classe&suppr=".$nomdelaClasse."');</script>";
            }
           
            else{
                echo "<script type='text/javascript'>document.location.replace('index.php?page=classe&selec');</script>";
            } 
        }
        
        
        //Afficher une alerte lorsque je supprime une classe
        if(isset($_GET['suppr'])){
          $suppr = $_GET['suppr'];
          echo'
            <div class="col-lg-12">                           
              <div class="card">
                <div class="card-close">
                  <div class="dropdown">
                    <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                      <i class="fa fa-ellipsis-v"></i>
                    </button>
                    <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
                      <a href="#" class="dropdown-item remove"><i class="fa fa-times"></i>Close</a>
                    </div>
                  </div>
                </div>
                <div class="alert alert-success" style="margin: 0px; padding: 18px; height: 60px; border-radius: 0px;" role="alert">Suppression de la classe <strong>'.$suppr.' </strong> réussie</div>
              </div>
            </div>';
        }
        
        //Afficher une alerte problèmme de sélection
        if(isset($_GET['selec'])){
          echo'
            <div class="col-lg-12">                           
              <div class="card">
                <div class="card-close">
                  <div class="dropdown">
                    <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                      <i class="fa fa-ellipsis-v"></i>
                    </button>
                    <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
                      <a href="#" class="dropdown-item remove"><i class="fa fa-times"></i>Close</a>
                    </div>
                  </div>
                </div>
                <div class="alert alert-danger" style="margin: 0px; padding: 18px; height: 60px; border-radius: 0px;" role="alert">Veuillez selectionner une classe !</div>
              </div>
            </div>';
        }
        
        
         
        
        $dateyear =  date("Y");
        $dateyear1 = $dateyear+1;
        
         $professeur = new professeur($db);
            $listeprof = $professeur->selectAll();

            $eleve = new eleve($db);
            $listeeleve = $eleve->selectAll();
        echo'
        <div class="col-lg-12" style="margin-top: 30px;">                           
          <div class="card">

            <div class="card-close">
              <div class="dropdown">
                <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                  <i class="fa fa-ellipsis-v"></i>
                </button>
                <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
                  <a href="#" class="dropdown-item remove"><i class="fa fa-times"></i>Close</a>
                </div>
              </div>
            </div>

            <div class="card-header d-flex align-items-center">
                <h3 class="h4">Création d\'une Classe</h3>
            </div>

            <form class="login-container" method="post" action= "index.php?page=classe" enctype="multipart/form-data">
                <div class="row" style="margin-top: 18px; padding: 10px;">
                  
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nom</label>
                      <input required="" type="text" class="form-control" id="exampleInputEmail1" name="nomClasse" aria-describedby="emailHelp"  >
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Année début</label>
                        <input required="" aria-describedby="emailHelp"  type="number" step="1" value="'.$dateyear.'" min="2000" max="2100" class="form-control" id="exampleInputEmail1" name="anneeDeb">
                    </div>
                  </div>
                  
                 
                   <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Année fin</label>
                        <input required="" aria-describedby="emailHelp"  type="number" step="1" value="'.$dateyear1.'" min="2000" max="2100" class="form-control" id="exampleInputEmail1" name="anneeFin">
                    </div>
                  </div>
                  
                  <div class="col-md-12">
                    <div class="form-group">
                    <!-- http://loudev.com/ -->
                    <select id="pre-selected-options"  name="listeclasse[]" multiple="multiple">
                  <optgroup label="Professeurs">';
                    foreach($listeprof as $unProf){
                        echo'<option style="text-transform: uppercase;" value="professeur|'.utf8_encode($unProf['pseudo'] ).'" >'.utf8_encode($unProf['nom'] ).' '.utf8_encode($unProf['prenom'] ).'</option>';
                    }
                    echo' 
                  </optgroup>
                  
                  <optgroup label="Élèves">';
                    foreach($listeeleve as $unEleve){
                        echo'<option style="text-transform: uppercase;" value="eleve|'.utf8_encode($unEleve['pseudo'] ).'">'.utf8_encode($unEleve['nom'] ).' '.utf8_encode($unEleve['prenom'] ).'</option>';
                    }
                    echo' 
                  </optgroup>
                </select>
                 </div>
                  </div>

                </div>
                <div class="form-group">
                  <p style="text-align: center;"><input input type="submit" id="btAjouter" name="btAjouter" value="Ajouter" class="mx-sm-3 btn btn-primary"></p>
                </div>
            </form>';
            
            echo'
            

          </div>
        </div>';
        
        
            
        $classe = new classe($db);
        $listeclasse = $classe->selectAll();
  
  
        //Afficher la liste des classes
        echo'
          <div class="col-lg-12">                           
            <div class="card">
              <div class="card-close">
                <div class="dropdown">
                  <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                    <i class="fa fa-ellipsis-v"></i>
                  </button>
                  <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
                    <a href="#" class="dropdown-item remove"><i class="fa fa-times"></i>Close</a>
                  </div>
                </div>
              </div>

              <div class="card-header d-flex align-items-center">
                  <h3 class="h4">Liste des Classes</h3>
              </div>

              <form method="post" action="index.php?page=classe">
                <table class="table table-striped" id="myTable" class="tablesorter" cellspacing="1" >
                  <thead>
                    <tr id="titre">
                      <th>Nom de la classe</th>
                      <th>Début de l\'année</th>
                      <th>Fin de l\'année</th>
                      <th>Selectionner</th>
                    </tr>
                  </thead>
                  <TBODY>
                  ';
                  
                  //Pour chaque administrateur, on affiche ...
                  foreach($listeclasse as $uneClasse){
                      echo '<tr>';
                        echo '<td>'.utf8_encode($uneClasse['nomClasse'] ).'</td>'; 
                        echo '<td>'.utf8_encode($uneClasse['anneeDeb'] ).'</td>'; 
                        echo '<td>'.utf8_encode($uneClasse['anneeFin'] ).'</td>'; 
                        echo '<td> <input type="radio" name="cocher[]" value="'.$uneClasse['numClasse'].'"> </td>';
                      echo '</tr>';
                  }

                echo '
                  </TBODY>
                    </table>
                     <p style="text-align: center;">
                       <input input type="submit" id="btSupprimer" name="btSupprimer" value="Supprimer" class="mx-sm-3 btn btn-primary">
                       <input input type="submit" id="btModifier" name="btModifier" value="Modifier" class="mx-sm-3 btn btn-primary">
                     </p>
                    </form>
                  </div>
                </div>';
    }
}}


?>