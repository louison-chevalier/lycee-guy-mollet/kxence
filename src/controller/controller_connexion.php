 <?php

function gestionconnect($db){


 $connexion = new connexion($db);
  $listeconnexion = $connexion->selectAll();
  
  
 echo '
        <div class="content-inner">
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Connexion au site Kxence</h2>
            </div>
          </header>
          
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                
                <div class="col-lg-12">                           
                  <div class="card">
                  

                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">LISTE DES CONNEXIONS</h3>
                    </div>
                    
                     <table class="table table-striped" id="myTable" class="tablesorter" cellspacing="1" >
            <thead>
              <tr id="titre">
                <th>Pseudo</th>
                <th>Rôle</th>
                <th>IP</th>
                <th>Date</th>
                <th>Navigateur</th>
              </tr>
            </thead>
            <TBODY>
            ';

            //Pour chaque administrateur, on affiche ...
            foreach($listeconnexion as $uneConnex){
                echo '<tr>';
                  echo '<td>'.utf8_encode($uneConnex['pseudo'] ).'</td>'; 
                  echo '<td>'.utf8_encode($uneConnex['role'] ).'</td>'; 
                  echo '<td>'.utf8_encode($uneConnex['remoteip'] ).'</td>'; 
                   echo '<td>'.utf8_encode($uneConnex['date'] ).'</td>';
                  echo '<td class="col-md-4">'.utf8_encode($uneConnex['navigateur'] ).'</td>';
                echo '</tr>';
            }


echo' </TBODY>
              </table>
                       

                  </div>
                </div>
          </section>
          ';
          
          }

?>