 <?php

 /*****************************************************************************/
/******************************** GESTION ELEVES ******************************/
/******************************************************************************/

function eleves($db){

//Header de la page
echo'
  <div class="content-inner">
    <header class="page-header">
      <div class="container-fluid">
        <h2 class="no-margin-bottom">Gestion des Eleves</h2>
      </div>
    </header>

    <section class="forms"> 
      <div class="container-fluid">
        <div class="row">';



//Lorsque on a appuyer sur le bouton modifier
if(isset($_POST['btModifier'])){

  //On récupere ce qui est cocher
  if (isset($_POST['cocher'])){
    $liste = $_POST['cocher'];
    if($liste != NULL){
      $eleve = new eleve($db);

      //Pour le premier pseudo de la liste
      foreach($liste as $pseudo){
        $eleve = $eleve->selectOne($pseudo);
        $pseudo = $eleve['pseudo'];
        echo'
          <div class="col-lg-12">                           
            <div class="card">

              <div class="card-close">
                <div class="dropdown">
                  <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                    <i class="fa fa-ellipsis-v"></i>
                  </button>
                  <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
                    <a href="#" class="dropdown-item remove"><i class="fa fa-times"></i>Close</a>
                  </div>
                </div>
              </div>

              <div class="card-header d-flex align-items-center">
                  <h3 class="h4">Modifier eleves : '.$pseudo .'</h3>
              </div>

              <form class="login-container" method="post" action= "index.php?page=eleves" enctype="multipart/form-data">
                  <div class="row" style="margin-top: 18px; padding: 10px;">
                  <input type="hidden" name="pseudo"  value="'.$pseudo.'" />
                    
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Mail  : <strong>'.$eleve['mail'].'</strong></label>
                        <input  type="email" class="form-control" name="mail" aria-describedby="emailHelp"  >
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nom : <strong>'.$eleve['nom'].'</strong></label>
                        <input  type="text" class="form-control" id="exampleInputEmail1" name="nom" aria-describedby="emailHelp" >
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Prenom : <strong>'.$eleve['prenom'].'</strong></label>
                          <input  type="text" class="form-control" id="exampleInputEmail1" name="prenom" aria-describedby="emailHelp" >
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Mot de passe : <strong></strong></label>
                        <input type="text" class="form-control" name="mdp" id="exampleInputEmail1" aria-describedby="emailHelp">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Date de naissance : <strong>'.$eleve['anniversaire'].'</strong></label>
                        <input type="text"  id="datepicker" class="form-control" name="anniversaire" aria-describedby="emailHelp" >
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Photo : <strong>'.$eleve['photo'].'</strong></label>
                        <input type="file" class="form-control" name="photo2" id="exampleInputEmail1" aria-describedby="emailHelp" >
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <p style="text-align: center;"><input input type="submit" id="btValider" name="btValider" value="Valider" class="mx-sm-3 btn btn-primary"></p>
                  </div>
              </form>
            </div>
          </div>';
      }
    }
  }
  else{
     echo "<script type='text/javascript'>document.location.replace('index.php?page=eleves&selec');</script>";
  } 
}


else{

  //Afficher une alerte lorsque je modifie un eleve
  if(isset($_POST['btValider'])){

      $pseudo = $_POST['pseudo'];

      $eleve = new eleve($db);
      $eleve = $eleve->selectOne($pseudo);
      $mail1 = $eleve['mail'];
      $nom1 = $eleve['nom'];
      $prenom1 = $eleve['prenom'];
      $mdp1 = $eleve['mdp'];
      $photo1 = $eleve['photo'];
      $anniversaire1 = $eleve['anniversaire'];

      $mail = $_POST['mail'];
      $nom = $_POST['nom'];
      $prenom = $_POST['prenom'];
      $mdp = sha1($_POST['mdp']);
      $anniversaire = $_POST['anniversaire'];


      if ($mail == NULL){
        $mail = $mail1;
      }
      if ($nom == NULL){
        $nom = $nom1;
      }
      if ($prenom == NULL){
        $prenom = $prenom1;
      }
      if ($mdp == NULL){
        $mdp = $mdp1;
      }
      if ($anniversaire == NULL){
        $anniversaire = $anniversaire1;
      }
      

      if (substr(strrchr($_FILES['photo2']['name'], '.'), 1)!=NULL){
        //extensions autorisées
        $extensions_ok = array('png', 'gif', 'jpg', 'jpeg');
        //taille maximun de la photo
        $taille_max = 500000;
        //répertoire ou sera stoker la photo
        $dest_dossier = '/var/www/html/kxence/assets/profils/eleves/';

        //On verifie que la photo est dans le bon format
        //Si "NON" on affiche une alerte
        if( !in_array( substr(strrchr($_FILES['photo2']['name'], '.'), 1), $extensions_ok ) ){
          echo '
                <div class="col-lg-12">                           
                  <div class="card">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                          <i class="fa fa-ellipsis-v"></i>
                        </button>
                        <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
                          <a href="#" class="dropdown-item remove"><i class="fa fa-times"></i>Close</a>
                        </div>
                      </div>
                    </div>
                    <div  class="alert alert-danger" style="margin: 0px; padding: 18px; height: 60px; border-radius: 0px;" role="alert">Veuillez sélectionner un fichier de type png, gif ou jpg !</div>
                  </div>
                </div>';
        }
        //Si "OUI" ...
        else{
          //On vérifie que la photo respecte la taille max autorisé
          //Si "NON" on affiche une alerte
          if( file_exists($_FILES['photo2']['tmp_name'])&& (filesize($_FILES['photo2']['tmp_name']))>$taille_max){
            echo '
                <div class="col-lg-12">                           
                  <div class="card">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                          <i class="fa fa-ellipsis-v"></i>
                        </button>
                        <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
                          <a href="#" class="dropdown-item remove"><i class="fa fa-times"></i>Close</a>
                        </div>
                      </div>
                    </div>
                    <div  class="alert alert-danger" style="margin: 0px; padding: 18px; height: 60px; border-radius: 0px;" role="alert">Votre fichier doit faire moins de 500Ko !</div>
                  </div>
                </div>';
          }

          //Si "OUI" on affiche une alerte ...
          else{
            // copie du fichier
            $dest_fichier = basename($_FILES['photo2']['name']); // formatage nom fichier
            // enlever les accents
            $dest_fichier=strtr($dest_fichier,'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùú ûüýÿ','AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
            // remplacer les caractères autres que lettres, chiffres et point par _
            $dest_fichier = preg_replace('/([^.a-z0-9]+)/i', '_', $dest_fichier);
            // copie du fichier
            move_uploaded_file($_FILES['photo2']['tmp_name'],$dest_dossier . $dest_fichier);
            Unlink('../kxence/assets/profils/eleves/'.$photo1.'');
            $photo = $dest_fichier;

          }
        }
      }

      else{
         $photo = $photo1;
      }

            $eleve = new eleve($db);
            $nb = $eleve->updateAll($pseudo, $mail, $nom, $prenom, $mdp, $photo, $anniversaire);

              //Si il y a une erreur, on affiche une alerte     
              if ($nb!=1){
                echo '
                  <div class="col-lg-12">                           
                    <div class="card">
                      <div class="card-close">
                        <div class="dropdown">
                          <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                            <i class="fa fa-ellipsis-v"></i>
                          </button>
                          <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
                            <a href="#" class="dropdown-item remove"><i class="fa fa-times"></i>Close</a>
                          </div>
                        </div>
                      </div>
                    <div  class="alert alert-danger" style="margin: 0px; padding: 18px; height: 60px; border-radius: 0px;" role="alert">Erreur d\'insertion</div>
                  </div>
                </div>';
              }

              //Si il n'y a pas d'erreur, on affiche une alerte
              else{
                echo'
                  <div class="col-lg-12">                           
                    <div class="card">
                      <div class="card-close">
                        <div class="dropdown">
                          <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                            <i class="fa fa-ellipsis-v"></i>
                          </button>
                          <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
                            <a href="#" class="dropdown-item remove"><i class="fa fa-times"></i>Close</a>
                          </div>
                        </div>
                      </div>
                      <div class="alert alert-success" style="margin: 0px; padding: 18px; height: 60px; border-radius: 0px;" role="alert">Modification réussi</div>
                    </div>
                  </div>';
              }
    }

     












  //Afficher une alerte lorsque je supprime un eleve
  if(isset($_GET['suppr'])){
    $suppr = $_GET['suppr'];
    echo'
      <div class="col-lg-12">                           
        <div class="card">
          <div class="card-close">
            <div class="dropdown">
              <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                <i class="fa fa-ellipsis-v"></i>
              </button>
              <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
                <a href="#" class="dropdown-item remove"><i class="fa fa-times"></i>Close</a>
              </div>
            </div>
          </div>
          <div class="alert alert-success" style="margin: 0px; padding: 18px; height: 60px; border-radius: 0px;" role="alert">Suppression du compte <strong>'.$suppr.' </strong> réussi</div>
        </div>
      </div>';
  }


  //Afficher une alerte problèmme de sélection
  if(isset($_GET['selec'])){
    echo'
      <div class="col-lg-12">                           
        <div class="card">
          <div class="card-close">
            <div class="dropdown">
              <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                <i class="fa fa-ellipsis-v"></i>
              </button>
              <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
                <a href="#" class="dropdown-item remove"><i class="fa fa-times"></i>Close</a>
              </div>
            </div>
          </div>
          <div class="alert alert-danger" style="margin: 0px; padding: 18px; height: 60px; border-radius: 0px;" role="alert">Veuillez selectionner un élèves !</div>
        </div>
      </div>';
  }



  //Permet d'ajouter un eleve
  //Si il y a un fichier dans le champ photo
  if(isset($_FILES['photo'])){
    //extensions autorisées
    $extensions_ok = array('png', 'gif', 'jpg', 'jpeg');
    //taille maximun de la photo
    $taille_max = 500000;
    //répertoire ou sera stoker la photo
    $dest_dossier = '/var/www/html/kxence/assets/profils/eleves/';

    //On verifie que la photo est dans le bon format
    //Si "NON" on affiche une alerte
    if( !in_array( substr(strrchr($_FILES['photo']['name'], '.'), 1), $extensions_ok ) ){
      echo '
            <div class="col-lg-12">                           
              <div class="card">
                <div class="card-close">
                  <div class="dropdown">
                    <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                      <i class="fa fa-ellipsis-v"></i>
                    </button>
                    <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
                      <a href="#" class="dropdown-item remove"><i class="fa fa-times"></i>Close</a>
                    </div>
                  </div>
                </div>
                <div  class="alert alert-danger" style="margin: 0px; padding: 18px; height: 60px; border-radius: 0px;" role="alert">Veuillez sélectionner un fichier de type png, gif ou jpg !</div>
              </div>
            </div>';
    }
    
    //Si "OUI" ...
    else{
      //On vérifie que la photo respecte la taille max autorisé
      //Si "NON" on affiche une alerte
      if( file_exists($_FILES['photo']['tmp_name'])&& (filesize($_FILES['photo']['tmp_name']))>$taille_max){
        echo '
            <div class="col-lg-12">                           
              <div class="card">
                <div class="card-close">
                  <div class="dropdown">
                    <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                      <i class="fa fa-ellipsis-v"></i>
                    </button>
                    <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
                      <a href="#" class="dropdown-item remove"><i class="fa fa-times"></i>Close</a>
                    </div>
                  </div>
                </div>
                <div  class="alert alert-danger" style="margin: 0px; padding: 18px; height: 60px; border-radius: 0px;" role="alert">Votre fichier doit faire moins de 500Ko !</div>
              </div>
            </div>';
      }

      //Si "OUI" on affiche une alerte ...
      else{
        // copie du fichier
        $dest_fichier = basename($_FILES['photo']['name']); // formatage nom fichier
        // enlever les accents
        $dest_fichier=strtr($dest_fichier,'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùú ûüýÿ','AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
        // remplacer les caractères autres que lettres, chiffres et point par _
        $dest_fichier = preg_replace('/([^.a-z0-9]+)/i', '_', $dest_fichier);
        // copie du fichier
        move_uploaded_file($_FILES['photo']['tmp_name'],$dest_dossier . $dest_fichier);
                  
        $photo = $dest_fichier;
        $eleve = new eleve($db);
                  
        $mail = $_POST['mail'];
        $nom = $_POST['nom'];
        $prenom = $_POST['prenom'];
        $mdp= sha1($_POST['mdp']);
        $anniversaire = $_POST['anniversaire'];
        
        $d = date('Y');
        $a = $d{2};
        $t = $d{3};
        $e = ''.$a.''.$t.'';
        
        $p = $prenom{0};
        $pseudo = 'e'.$p.''.$nom.''.$e.'';
        
        //On ajoute à la base de donnée                 
        $nb = $eleve->insertAll($pseudo, $mail, $nom, $prenom, $mdp, $photo, $anniversaire);          
        
        //Si il y a une erreur, on affiche une alerte     
        if ($nb!=1){
          echo '
            <div class="col-lg-12">                           
              <div class="card">
                <div class="card-close">
                  <div class="dropdown">
                    <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                      <i class="fa fa-ellipsis-v"></i>
                    </button>
                    <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
                      <a href="#" class="dropdown-item remove"><i class="fa fa-times"></i>Close</a>
                    </div>
                  </div>
                </div>
                <div  class="alert alert-danger" style="margin: 0px; padding: 18px; height: 60px; border-radius: 0px;" role="alert">Erreur d\'insertion</div>
              </div>
            </div>';
        }

        //Si il n'y a pas d'erreur, on affiche une alerte
        else{
          echo'
            <div class="col-lg-12">                           
              <div class="card">
                <div class="card-close">
                  <div class="dropdown">
                    <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                      <i class="fa fa-ellipsis-v"></i>
                    </button>
                    <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
                      <a href="#" class="dropdown-item remove"><i class="fa fa-times"></i>Close</a>
                    </div>
                  </div>
                </div>
                <div class="alert alert-success" style="margin: 0px; padding: 18px; height: 60px; border-radius: 0px;" role="alert">Création du compte <strong>'.$pseudo.' </strong> réussi</div>
              </div>
            </div>';
        }
      }
    }
  }

  //On appelle la fonction genererMdp() qui génére un mot de passe de 12 caractères
  $genpass = genererMdp(12);

  //Formulaire pour ajouter un eleve
  echo'
    <div class="col-lg-12">                           
      <div class="card">

        <div class="card-close">
          <div class="dropdown">
            <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
              <i class="fa fa-ellipsis-v"></i>
            </button>
            <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
              <a href="#" class="dropdown-item remove"><i class="fa fa-times"></i>Close</a>
            </div>
          </div>
        </div>

        <div class="card-header d-flex align-items-center">
            <h3 class="h4">Ajouter un Élève</h3>
        </div>

        <form class="login-container" method="post" action= "index.php?page=eleves" enctype="multipart/form-data">
            <div class="row" style="margin-top: 18px; padding: 10px;">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="exampleInputEmail1">Mail</label>
                  <input required="" type="email" class="form-control" id="exampleInputEmail1" name="mail" aria-describedby="emailHelp"  >
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="exampleInputEmail1">Nom</label>
                  <input required="" type="text" class="form-control" id="exampleInputEmail1" name="nom" aria-describedby="emailHelp"  >
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="exampleInputEmail1">Prenom</label>
                    <input required="" type="text" class="form-control" id="exampleInputEmail1" name="prenom" aria-describedby="emailHelp"  >
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="exampleInputEmail1">Mot de passe</label>
                  <input required="" type="password" class="form-control" value="'.$genpass.'" name="mdp" id="exampleInputEmail1" aria-describedby="emailHelp"  >
                  <label for="optionsRadios2">(Le mot de passe générer par default est '.$genpass.')</label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="exampleInputEmail1">Date de naissance</label>
                  <input required="" type="text"  id="datepicker" class="form-control" name="anniversaire" aria-describedby="emailHelp">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="exampleInputEmail1">Photo</label>
                  <input required="" type="file" class="form-control" name="photo" id="exampleInputEmail1" aria-describedby="emailHelp"  >
                </div>
              </div>
            </div>
            <div class="form-group">
              <p style="text-align: center;"><input input type="submit" id="btAjouter" name="btAjouter" value="Ajouter" class="mx-sm-3 btn btn-primary"></p>
            </div>
        </form>
      </div>
    </div>';


            
  $eleve = new eleve($db);
  $listeeleve = $eleve->selectAll();

  //Lorsque on a appuyer sur le bouton supprimer
  if(isset($_POST['btSupprimer'])){
    //On récupere ce qui est cocher
    if (isset($_POST['cocher'])){
      $liste = $_POST['cocher'];

        $eleve = new eleve($db);
        //Pour le premier pseudo de la liste
        foreach($liste as $pseudo){
          $eleve = $eleve->selectOne($pseudo);
          //on récupere le nom de la photo liée au pseudo
          $photo = $eleve['photo'];
          //on supprime la photo
          Unlink('assets/profils/eleves/'.$photo.'');
          //on supprime l'admin
          $nb = $eleve->deleteOne($pseudo);
          //On rafraichi la page, une nouvelle variable est placé en paramètre, elle récupére le pseudo de la personne supprimer
          //Pour ensuite afficher une alerte
          echo "<script type='text/javascript'>document.location.replace('index.php?page=eleves&suppr=".$pseudo."');</script>";
        }

      }
    else{
      echo "<script type='text/javascript'>document.location.replace('index.php?page=eleves&selec');</script>";
    } 
  }
  


  //CALCUL NOMBRE ELEVES
  $sql = "SELECT count(pseudo) from eleve";
  $req2 = $db->query ($sql);
  $red3 = $req2->fetch();
  $totaleleve = ($red3[0]);



  //Afficher la liste des eleves
  echo'
    <div class="col-lg-12">                           
      <div class="card">
        <div class="card-close">
          <div class="dropdown">
            <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
              <i class="fa fa-ellipsis-v"></i>
            </button>
            <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
              <a href="#" class="dropdown-item remove"><i class="fa fa-times"></i>Close</a>
            </div>
          </div>
        </div>

        <div class="card-header d-flex align-items-center">
            <h3 class="h4">Liste des Élèves</h3>
        </div>

        <form method="post" action="index.php?page=eleves">
          <table class="table table-striped" id="myTable" class="tablesorter" cellspacing="1" >
            <thead>
              <tr id="titre">
                <th>Photo</th>
                <th>Pseudo</th>
                <th>Nom</th>
                <th>Prenom</th>
                <th>Mail</th>
                <th>Selectionner</th>';

              echo'
              </tr>
            </thead>
            <TBODY>
            ';

            //Pour chaque professeur, on affiche ...
            foreach($listeeleve as $unEleve){
                echo '<tr>';
                  echo '<td><img height="70px;" src="assets/profils/eleves/'.utf8_encode($unEleve['photo'] ).'"></td>';
                  echo '<td>'.utf8_encode($unEleve['pseudo'] ).'</td>'; 
                  echo '<td>'.utf8_encode($unEleve['nom'] ).'</td>'; 
                  echo '<td>'.utf8_encode($unEleve['prenom'] ).'</td>'; 
                  echo '<td>'.utf8_encode($unEleve['mail'] ).'</td>';

                  //Permet de toujours avoir un professeur au minimun
                  echo '<td> <input type="radio" name="cocher[]" value="'.$unEleve['pseudo'].'"> </td>';
                echo '</tr>';
            }

          echo '
            </TBODY>
              </table>
               <p style="text-align: center;">
                 <input input type="submit" id="btSupprimer" name="btSupprimer" value="Supprimer" class="mx-sm-3 btn btn-primary">
                 <input input type="submit" id="btModifier" name="btModifier" value="Modifier" class="mx-sm-3 btn btn-primary">
               </p>
              </form>
            </div>
          </div>';

  }
}







 
 function tableeleves($db){
 echo '

        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Élèves</h2>
            </div>
          </header>
          <section class="tables">   
            <div class="container-fluid">
              <div class="row">
                <div class="col-lg-6">
                  <div class="card">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                        <div aria-labelledby="closeCard" class="dropdown-menu has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
                      </div>
                    </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Basic Table</h3>
                    </div>
                    ';
                    listeeleve($db);
                    echo'
                    <div class="card-body">
                      <table class="table">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Username</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <th scope="row">1</th>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                          </tr>
                          <tr>
                            <th scope="row">2</th>
                            <td>Jacob</td>
                            <td>Thornton</td>
                            <td>@fat</td>
                          </tr>
                          <tr>
                            <th scope="row">3</th>
                            <td>Larry</td>
                            <td>the Bird</td>
                            <td>@twitter</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="card">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                        <div aria-labelledby="closeCard" class="dropdown-menu has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
                      </div>
                    </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Striped Table</h3>
                    </div>
                    <div class="card-body">
                      <table class="table table-striped">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Username</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <th scope="row">1</th>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                          </tr>
                          <tr>
                            <th scope="row">2</th>
                            <td>Jacob</td>
                            <td>Thornton</td>
                            <td>@fat</td>
                          </tr>
                          <tr>
                            <th scope="row">3</th>
                            <td>Larry</td>
                            <td>the Bird</td>
                            <td>@twitter                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="card">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                        <div aria-labelledby="closeCard" class="dropdown-menu has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
                      </div>
                    </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Striped table with hover effect</h3>
                    </div>
                    <div class="card-body">
                      <table class="table table-striped table-hover">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Username</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <th scope="row">1</th>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                          </tr>
                          <tr>
                            <th scope="row">2</th>
                            <td>Jacob</td>
                            <td>Thornton</td>
                            <td>@fat</td>
                          </tr>
                          <tr>
                            <th scope="row">3</th>
                            <td>Larry</td>
                            <td>the Bird</td>
                            <td>@twitter                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="card">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                        <div aria-labelledby="closeCard" class="dropdown-menu has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
                      </div>
                    </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Compact Table</h3>
                    </div>
                    <div class="card-body">
                      <table class="table table-striped table-sm">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Username</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <th scope="row">1</th>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                          </tr>
                          <tr>
                            <th scope="row">2</th>
                            <td>Jacob</td>
                            <td>Thornton</td>
                            <td>@fat</td>
                          </tr>
                          <tr>
                            <th scope="row">3</th>
                            <td>Larry</td>
                            <td>the Bird</td>
                            <td>@twitter      </td>
                          </tr>
                          <tr>
                            <th scope="row">4</th>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                          </tr>
                          <tr>
                            <th scope="row">5</th>
                            <td>Jacob</td>
                            <td>Thornton</td>
                            <td>@fat</td>
                          </tr>
                          <tr>
                            <th scope="row">6</th>
                            <td>Larry</td>
                            <td>the Bird</td>
                            <td>@twitter                                                                              </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          ';
          }

function listeeleve($db){
    
    echo '<h2>Liste des Eleves </h2><br> ';
   
    $eleve = new eleve($db);
    $listeeleve = $eleve->selectAll();


    echo '<table class="table table-striped id="client" >';
        echo '<thead>';
            echo '<tr id="titre">';
                echo ' <th>Pseudo</th>';
                echo ' <th>Mail</th>';
                echo ' <th >Nom</th>';
                echo ' <th>Prenom</th>';
            echo '</tr>';
        echo '</thead>';
        echo '<TBODY>';
            foreach($listeeleve as $eleve){
                echo '<tr>';
                    echo '<td>'.utf8_encode($eleve['pseudo'] ).'</td>'; 
                    echo '<td>'.utf8_encode($eleve['mail'] ).'</td>'; 
                    echo '<td>'.utf8_encode($eleve['nom'] ).'</td>'; 
                    echo '<td>0'.utf8_encode($eleve['prenom'] ).'</td>'; 
                echo '</tr>';
            }
        echo '</TBODY>';
    echo '</table>';

 }


?>