 <?php

function filtre(){

// J'ajoute le liens dans la liste
// des sites autorisés
if(isset($_POST['btsite'])){
    $site = $_POST['site'];    
}
    

// affiche la page
echo '
<div class="content-inner">

    <!-- HEADER -->
    <header class="page-header">
        <div class="container-fluid">
            <h2 class="no-margin-bottom">Filtre accès web</h2>
        </div>
    </header>
    

    <!-- FORMULAIRE AJOUTER SITE -->
    <section class="forms"> 
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">                           
                    <div class="card">
                        
                        <div class="card-close">
                            <div class="dropdown">
                                <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                                <div aria-labelledby="closeCard" class="dropdown-menu has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a></div>
                            </div>
                        </div>

                        <div class="card-header d-flex align-items-center">
                            <h3 class="h4">AJOUT SITE WEB</h3>
                        </div>
                    
                        <form class="login-container" method="post" action= "index.php?page=filtre" enctype="multipart/form-data">
                            <div class="row" style="margin-top: 18px; padding: 10px;">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Nom du site</label>
                                        <input required="" type="text" class="form-control" id="exampleInputEmail1" name="site" aria-describedby="emailHelp"  >
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <p style="text-align: center;"><input input type="submit" id="btsite" name="btsite" value="Ajouter" class="mx-sm-3 btn btn-primary"></p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
          


    <!-- LISTE DES SITES AUTORISÉS -->
    <section class="projects no-padding-top" style="margin-top: 0px;">
        <div class="container-fluid">
        
            <!-- AFFICHE SITE -->
            <div class="project">
                <div class="row bg-white has-shadow">
                    <div class="left-col col-lg-6 d-flex align-items-center justify-content-between">
                        <div class="project-title d-flex align-items-center">
                            <div class="text">
                                <h3 class="h4"><a target="_blanck" href="http://www.alquines.net">www.alquines.net</a></h3>
                            </div>
                        </div>
                        <div class="project-date"><span class="hidden-sm-down">Today at 4:24 AM</span></div>
                    </div>
                    <div class="right-col col-lg-6 d-flex align-items-center">
                        <div class="time"><i class="fa fa-clock-o"></i>12:00 PM </div>
                        <div class="comments"><i class="fa fa-comment-o"></i>20</div>
                        <div class="project-progress">
                            <div class="progress">
                                <div role="progressbar" style="width: 45%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-red"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </section>';

}

?>