<?php

/******************************************************************************/
/******************************* PAGE DE LOGIN ********************************/
/******************************************************************************/

function Login($db){

//Initialise les variables
$role = "";
$color = "";
$msg = "";
  

//Lorque que l'on veut se connecter
if(isset($_POST['btLogin'])){
    //Je récupére le role
    $role = $_POST['role'];
    
    //Si la personne veut se connecter en admin
    if ($role == 'admin'){
        //Je récupére le pseudo
        $pseudo = $_POST['pseudo'] ;
        //Si pseudo existe alors
        if (!empty($pseudo)){
            //je recupere le mot de passe
            //que je crypte en sha1
            $mdp= sha1($_POST['mdp']);
            //Si le mot de passe existe alors
            if (!empty($mdp)){
                //Appel de la class Admin
                $admin = new Admin($db);
                // On regarde si l’utilisateur est retourné en appelant la 
                // méthode selectConnexion et en lui donnant un login et
                // un mot de passe
                $unAdmin = $admin->selectConnexion($pseudo, $mdp);
                // Si un utilisateur est renvoyé donc différent de false
                if ($unAdmin!=false){
                    // On sauvegarde le login dans la superglobale SESSION
                    $_SESSION['pseudo'] = $pseudo;
                    $_SESSION['role'] = "2";
                    
                    $role = 'admins';
                    //permet de récupérer l'addresse ip de l'utilisateur
                    $remoteip = $_SERVER['REMOTE_ADDR'];
                    
                    $navigateur = $_SERVER['HTTP_USER_AGENT'];
                    //Appel de la class connexion
                    $connexion = new connexion($db);
                    //On ajoute à la base de donnée
                    //Permet de garder un historique des connexions
                    $nb = $connexion->insertAll($pseudo, $role, $remoteip, $navigateur);          
                }
            }
        }
    }
  
    //Si la personne veut se connecter en professeur
    elseif ($role == 'prof'){
        //Je récupére le pseudo
        $pseudo = $_POST['pseudo'] ;
        //Si pseudo existe alors
        if (!empty($pseudo)){
            //je recupere le mot de passe
            //que je crypte en sha1
            $mdp= sha1($_POST['mdp']);
            //Si le mot de passe existe alors
            if (!empty($mdp)){
                //Appel de la class professeur
                $professeur = new Professeur($db);
                // On regarde si l’utilisateur est retourné en appelant la 
                // méthode selectConnexion
                // et en lui donnant un login et un mot de passe
                $unProfesseur = $professeur->selectConnexion($pseudo, $mdp);
                // Si un utilisateur est renvoyé donc différent de false
                if ($unProfesseur!=false){
                    // On sauvegarde le login dans la superglobale SESSION
                    $_SESSION['pseudo'] = $pseudo;
                    $_SESSION['role'] = "1";
                    
                    $role = 'professeurs';
                    //permet de récupérer l'addresse ip de l'utilisateur
                    $remoteip = $_SERVER['REMOTE_ADDR'];
                    
                    $navigateur = $_SERVER['HTTP_USER_AGENT'];
                    //Appel de la class connexion
                    $connexion = new connexion($db);
                    //On ajoute à la base de donnée
                    //Permet de garder un historique des connexions
                    $nb = $connexion->insertAll($pseudo, $role, $remoteip, $navigateur);    
                }
            }
        }
    }
    //Sinon les variables prennent les valeurs ...
    $erreur = "Vérifier votre pseudo et mot de passe !";
    $color = "red";
} 

//Si l'utilissateur est connecté
if (isset($_SESSION['pseudo'])){
    //Si c'est un compte de professeur
    if ($_SESSION['role'] == "1") {
        //Je le redirige vers la page d\'accueil pour les professeurs
        echo "<script type='text/javascript'>document.location.replace('index.php?page=Accueil');</script>";
    }
    
    //Si c'est un compte de administrateur
    if ($_SESSION['role'] == "2") {
        //Je le redirige vers la page d\'accueil pour les administrateurs
        echo "<script type='text/javascript'>document.location.replace('index.php?page=Accueil');</script>";
    }
}

//Lorsque l'utilisateur se deconnecte..
if(isset($_GET['deco'])){
    $_SESSION = array();
    //je supprime les données dans $_SESSION
    session_destroy();
}

//Affiche la page
echo '
<body>
    <div class="page login-page">
        <div class="container d-flex align-items-center">
            <div class="form-holder has-shadow">
                <div class="row">
                
                    <!-- PARTIE DE GAUCHE -->
                    <div class="col-lg-6">
                        <div class="info d-flex align-items-center">
                            <div class="content">
                                <div class="logo">
                                    <h1>KXENCE</h1>
                                </div>
                                <p>Bienvenue dans l\'espace des responsables.</p>
                            </div>
                        </div>
                     </div>

                    <!-- PARTIE DE DROITE -->
                    <div class="col-lg-6 bg-white">
                        <div class="form d-flex align-items-center">
                            <div class="content">
                            
                                <form id="login-form" action="index.php?page=Login" method="post">
                                    <div class="form-group">
                                        <p style="margin-bottom: 0px; padding-bottom: 0px; color:'.$color.';">Pseudo :</p>
                                        <input id="pseudo" type="text" name="pseudo" required="" class="input-material">
                                    </div>
                                    <div class="form-group">
                                        <p style="margin-bottom: 0px; padding-bottom: 0px; color:'.$color.';">Mot de passe :</p>
                                        <input id="mdp" type="password" name="mdp" required="" class="input-material">
                                    </div>
                                    <div class="form-group">
                                      <select id="role" name="role">
                                          <option value="admin">Administrateur</option>
                                          <option value="prof">Professeur</option>
                                      </select>
                                    </div>
                                    <div class="form-group">
                                      <input type="submit" name="btLogin" class="btn btn-primary" id="btLogin" value="Login">
                                    </div>
                                </form>';

                                //Si il y a une erreur de mot passe ou pseudo...
                                if (isset($erreur)){
                                    //... afficher message erreur
                                    echo '<p style="color: red;">'.$erreur.'</p>';
                                }
                  
                                echo'
                                <a href="index.php?page=Passwd" class="forgot-pass">Mot de passe oublié</a>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Page Footer-->
    <footer class="main-footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                  <p>GORLIER Valentin - CHEVALIER Louison &copy; 2017-2018</p>
                </div>
                <div class="col-sm-6 text-right">
                  <p>KXENCE</p>
                </div>
            </div>
        </div>
    </footer>
</body>
';

}









/******************************************************************************/
/*************************PAGE MOT DE PASSE OUBLIÉ ****************************/
/******************************************************************************/

function Passwd($db){
    
//Initialise les variables
$color = "red";

//Lorque que l'on veut se récupérer son mot de passe
if(isset($_POST['pwd'])){
    //Je récupére le pseudo dans une variable
    $pseudo = $_POST['pseudo'] ;
    //Je récupére la date d'anniversaire dans une variable
    $anniversaire = $_POST['anniversaire'] ;
    //Je récupére la première lettre du pseudo
    //pour connaitre le role de l'utilisateur
    $r = substr("$pseudo", 0, 1);
    
    /********** CAPTCHA DE GOOGLE **********/
    // Ma clé privée
    $secret = "6LfygTsUAAAAAEx5YMsUrkexcUBF9iRC_MUDS1Bm";
    // Paramètre renvoyé par le recaptcha
    $response = $_POST['g-recaptcha-response'];
    // On récupère l'IP de l'utilisateur
    $remoteip = $_SERVER['REMOTE_ADDR'];
    $api_url = "https://www.google.com/recaptcha/api/siteverify?secret=" 
	    . $secret
	    . "&response=" . $response
	    . "&remoteip=" . $remoteip ;
    $decode = json_decode(file_get_contents($api_url), true);
    
    // L'utilisateur n'est pas un robot donc ...
    if ($decode['success'] == true) {
        
        //Si le role de l'utilisateur est admin alors ..
        if ($r == "a"){
            //Appel de la class admin
            $admin = new Admin($db);
            $unAdmin = $admin->mdpoublie($pseudo, $anniversaire);
            //Je récupère le mail de l'administrateur
            $mail = $unAdmin['mail'];
        }                                                                           
        
        //Si le role de l'utilisateur est élève alors ..
        if ($r == "e"){
            //Appel de la class eleve
            $eleve = new Eleve($db);
            //Requete sql
            $unEleve = $eleve->mdpoublie($pseudo, $anniversaire);
            //Je récupère le mail de l'élève
            $mail = $unEleve['mail'];
        }
        
        //Si le role de l'utilisateur est professeur alors ..
        if ($r == "p"){
            //Appel de la class professeur
            $professeur = new Professeur($db);
            //Requete sql
            $unProfesseur = $professeur->mdpoublie($pseudo, $anniversaire);
            //Je récupère le mail du professeur
            $mail = $unProfesseur['mail'];
        }
        
        if($mail != NULL){
            //J'utilisse la fonction qui permet de censurer une adresse mail
            //(de remplacer une partie de l'adresse par des point
            $envoiemail = censuremail($mail);
            echo '
            <body>
                <div class="page login-page">
                    <div class="container d-flex align-items-center">
                        <div class="form-holder has-shadow">
                            <div class="row">

                                <!-- PARTIE DE GAUCHE -->
                                <div class="col-lg-6">
                                    <div class="info d-flex align-items-center">
                                        <div class="content">
                                            <div class="logo">
                                                <h1>KXENCE</h1>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- PARTIE DE DROITE -->
                                <div class="col-lg-6 bg-white">
                                    <div class="form d-flex align-items-center">
                                        <div class="content">
                                            <a href="index.php?page=Accueil" class="forgot-pass"> &larr; retour</a>
                                            <p>Votre mot de passe a été envoyé à l\'adresse :<br> '.$envoiemail.'</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Page Footer-->
                <footer class="main-footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-6">
                              <p>GORLIER Valentin - CHEVALIER Louison &copy; 2017-2018</p>
                            </div>
                            <div class="col-sm-6 text-right">
                              <p>KXENCE</p>
                            </div>
                        </div>
                    </div>
                </footer>
            </body>';
        }
        else {
            Passwdform($color);
        }   
    }
    else {
       Passwdform($color);
    }
}
else{
    $color = "";
    Passwdform($color);
}

}





/******************************************************************************/
/*******************AFFICHAGE PAGE MOT DE PASSE OUBLIÉ ************************/
/******************************************************************************/

function Passwdform($color){
    echo'
    <body>
        <div class="page login-page">
            <div class="container d-flex align-items-center">
                <div class="form-holder has-shadow">
                    <div class="row">

                        <!-- PARTIE DE GAUCHE -->
                        <div class="col-lg-6">
                            <div class="info d-flex align-items-center">
                                <div class="content">
                                    <div class="logo">
                                        <h1>KXENCE</h1>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- PARTIE DE DROITE -->
                        <div class="col-lg-6 bg-white">
                            <div class="form d-flex align-items-center">
                                <div class="content">
                                    <a href="index.php?page=Accueil" class="forgot-pass"> &larr; retour</a>
                                    <form id="login-form" action="index.php?page=Passwd" method="post" style="margin-top:15px;">
                                    
                                        <h1 style="margin-bottom: 30px;">Mot de passe oublié </h1>
                                        
                                        <div class="form-group" ">
                                          <p style="margin-bottom: 0px; padding-bottom: 0px; color:'.$color.';">Saisir votre Pseudo :</p>
                                          <input id="pseudo" type="text" name="pseudo" required="" class="input-material">
                                        </div>

                                        <div class="form-group">
                                          <p style="margin-bottom: 0px; padding-bottom: 0px; color:'.$color.';">Saisir votre date de naissance :</p>
                                          <input type="text"  id="datepicker" class="input-material" required="" name="anniversaire" aria-describedby="emailHelp" >
                                        </div>
                                        
                                        <div class="g-recaptcha" data-sitekey="6LfygTsUAAAAAGXwlrs4glL8w_OGzy1UhrrySOcP" style="margin-bottom : 20px;"></div>
                                        
                                        <div class="form-group">
                                          <input type="submit" name="pwd" class="btn btn-primary" id="pwd" value="Valider">
                                        </div>
                                        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page Footer-->
        <footer class="main-footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                      <p>GORLIER Valentin - CHEVALIER Louison &copy; 2017-2018</p>
                    </div>
                    <div class="col-sm-6 text-right">
                      <p>KXENCE</p>
                    </div>
                </div>
            </div>
        </footer>
        <script src="assets/js/jquery-1.9.1.js"></script>
        <script src="assets/js/jquery-ui-1.10.2.custom.js"></script>
        <script type="text/javascript" src="assets/js/calendrier.js"></script>
        <script src="https://www.google.com/recaptcha/api.js"></script>
    </body>';
}

?>
