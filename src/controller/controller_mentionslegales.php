<?php

function mentions(){

//Header de la page
echo'
  <div class="content-inner">
    <header class="page-header">
      <div class="container-fluid">
        <h2 class="no-margin-bottom">Mentions Légales</h2>
      </div>
    </header>
    
    <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                
                <div class="col-lg-12" >                           
                  <div class="card" style="padding: 30px;">
                    

                    
                    <p style="text-align:justify"><strong>1. Présentation du site :<br />
 </strong><br />
Conformément aux dispositions des articles 6-III et 19 de la Loi n° 2004-575 du 21 juin 2004 pour la Confiance dans l\'économie numérique, dite L.C.E.N., nous portons à la connaissance des utilisateurs et visiteurs du site : Kxence les informations suivantes :</p>
<p style="text-align:justify"><strong>Informations légales :  </strong>
</br >
</br >
Statut du propriétaire : <strong>societe</strong><br />
Préfixe : <strong>SASU</strong><br />
Nom de la Société :<strong> KXENCE</strong><br />
Adresse : <strong> 57 Rue Bocquet Flochel 62000 Arras</strong><br />
Tél  : <strong>0672742798</strong><br />
Au Capital de :<strong> 299 301 900,00 €</strong><br />
SIRET :  <strong>46564962666466   </strong>R.C.S. :<strong> Maxence Roger</strong><br />
Numéro TVA intracommunautaire : <strong>FR 40 123456824 </strong><br />
Adresse de courrier électronique : <strong><a href="mailto:valentin.gorlier@hotmail.fr?subject=Contact ï¿½ partir des mentions lï¿½gales via le site kxence.fr">valentin.gorlier@hotmail.fr</a></strong> <br />
 <br />
Les Créateurs du site sont : <strong>Valentin GORLIER & Louison CHEVALIER</strong><br />
Le Responsable de la  publication est : <strong>CHEVALIER Louison</strong><br />
Contactez le responsable de la publication : <strong><a href="mailto:louison.chevalier@gmail.com?subject=Contact ï¿½ partir des mentions lï¿½gales via le site kxence.fr">louison.chevalier@gmail.com</a></strong><br />
Le responsable de la publication est une <strong>personne physique</strong><br />
<br />
Les Webmaster sont  : <strong>Louison CHEVALIER et Valentin GORLIER</strong><br />
Contactez le Webmaster : <strong><a href="mailto:louison.chevalier@gmail.com?subject=Contact ï¿½ partir des mentions lï¿½gales via le site kxence.fr">louison.chevalier@gmail.com</a></strong><br />
L\'hebergeur du site est : <strong>KXENCE Industries 57 Rue Bocquet Flochel 62000 Arras</strong><br />
<u><strong>CREDITS :</strong></u> les mentions légales ont étés générées par <strong><a href="https://www.generer-mentions-legales.com">mentions legales</a></strong><br />
 </p>
<p style="text-align:justify"><strong>2. Description des services fournis :</strong><br />
<br />
Le site Kxence a pour objet de fournir une information concernant l\'ensemble des activités de la société.<br />
Le proprietaire du site s\'efforce de fournir sur le site Kxence des informations aussi précises que possible. Toutefois, il ne pourra être tenue responsable des omissions, des inexactitudes et des carences dans la mise à jour, qu\'elles soient de son fait ou du fait des tiers partenaires qui lui fournissent ces informations.<br />
Tous les informations proposées sur le site Kxence sont données à titre indicatif, sont non exhaustives, et sont susceptibles d\'évoluer. Elles sont données sous réserve de modifications ayant été apportées depuis leur mise en ligne.<br />
 </p>

<p style="text-align:justify"><strong>3. Propriété intellectuelle et contrefaçons :</strong></p>

<p style="text-align:justify"><br />
Le proprietaire du site est propriétaire des droits de propriété intellectuelle ou détient les droits d\'usage sur tous les éléments accessibles sur le site, notamment les textes, images, graphismes, logo, icônes, sons, logiciels…<br />
Toute reproduction, représentation, modification, publication, adaptation totale ou partielle des éléments du site, quel que soit le moyen ou le procédé utilisé, est interdite, sauf autorisation écrite préalable à l\'email : <a href="mailto:louison.chevalier@gmail.com?subject=Contact ï¿½ partir des mentions lï¿½gales via le site kxence.fr"><strong>louison.chevalier@gmail.com</strong></a> .<br />
Toute exploitation non autorisée du site ou de l\'un quelconque de ces éléments qu\'il contient sera considérée comme constitutive d\'une contrefaçon et poursuivie conformément aux dispositions des articles L.335-2 et suivants du Code de Propriété Intellectuelle.<br />
 </p>

<p style="text-align:justify"><strong>4. Protection des biens et des personnes - gestion des données personnelles :</strong><br />
<br />
Utilisateur : Internaute se connectant, utilisant le site susnommé : Kxence <br />
En France, les données personnelles sont notamment protégées par la loi n° 78-87 du 6 janvier 1978, la loi n° 2004-801 du 6 août 2004, l\'article L. 226-13 du Code pénal et la Directive Européenne du 24 octobre 1995.</p>

<p style="text-align:justify">Sur le site Kxence, le proprietaire du site ne collecte des informations personnelles relatives à l\'utilisateur que pour le besoin de certains services proposés par le site Kxence. L\'utilisateur fournit ces informations en toute connaissance de cause, notamment lorsqu\'il procède par lui-même à leur saisie. Il est alors précisé à l\'utilisateur du site Kxence l\'obligation ou non de fournir ces informations.<br />
Conformément aux dispositions des articles 38 et suivants de la loi 78-17 du 6 janvier 1978 relative à l\'informatique, aux fichiers et aux libertés, tout utilisateur dispose d\'un droit d\'accès, de rectification, de suppression et d\'opposition aux données personnelles le concernant. Pour l\'exercer, adressez votre demande à Kxence par email : email du webmaster ou  en effectuant sa demande écrite et signée, accompagnée d\'une copie du titre d\'identité avec signature du titulaire de la pièce, en précisant l\'adresse à laquelle la réponse doit être envoyée.</p>

<p style="text-align:justify">Aucune information personnelle de l\'utilisateur du site Kxence n\'est publiée à l\'insu de l\'utilisateur, échangée, transférée, cédée ou vendue sur un support quelconque à des tiers. Seule l\'hypothèse du rachat du site Kxence à le proprietaire du site et de ses droits permettrait la transmission des dites informations à l\'éventuel acquéreur qui serait à son tour tenu de la même obligation de conservation et de modification des données vis à vis de l\'utilisateur du site Kxence.<br />
Le site Kxence est déclaré à la CNIL sous le numéro Aucune déclaration CNIL car pas de recueil de données personnelles.</p>

<p style="text-align:justify">Les bases de données sont protégées par les dispositions de la loi du 1er juillet 1998 transposant la directive 96/9 du 11 mars 1996 relative à la protection juridique des bases de données.</p>

                    </div>
                </div>
          </section>


   
    



';


}