 <?php

/******************************************************************************/
/****************************** GESTION PROFESSEURS ***************************/
/******************************************************************************/

function professeur($db){

//Header de la page
echo'
  <div class="content-inner">
    <header class="page-header">
      <div class="container-fluid">
        <h2 class="no-margin-bottom">Gestion des Professeurs</h2>
      </div>
    </header>

    <section class="forms"> 
      <div class="container-fluid">
        <div class="row">';



//Lorsque on a appuyer sur le bouton modifier
if(isset($_POST['btModifier'])){

  //On récupere ce qui est cocher
  if (isset($_POST['cocher'])){
    $liste = $_POST['cocher'];
    if($liste != NULL){
      $professeur = new professeur($db);

      //Pour le premier pseudo de la liste
      foreach($liste as $pseudo){
        $professeur = $professeur->selectOne($pseudo);
        $pseudo = $professeur['pseudo'];
        echo'
          <div class="col-lg-12">                           
            <div class="card">

              <div class="card-close">
                <div class="dropdown">
                  <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                    <i class="fa fa-ellipsis-v"></i>
                  </button>
                  <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
                    <a href="#" class="dropdown-item remove"><i class="fa fa-times"></i>Close</a>
                  </div>
                </div>
              </div>

              <div class="card-header d-flex align-items-center">
                  <h3 class="h4">Modifier professeur : '.$pseudo .'</h3>
              </div>

              <form class="login-container" method="post" action= "index.php?page=professeur" enctype="multipart/form-data">
                  <div class="row" style="margin-top: 18px; padding: 10px;">
                  <input type="hidden" name="pseudo"  value="'.$pseudo.'" />
                    
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Mail  : <strong>'.$professeur['mail'].'</strong></label>
                        <input  type="email" class="form-control" name="mail" aria-describedby="emailHelp"  >
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nom : <strong>'.$professeur['nom'].'</strong></label>
                        <input  type="text" class="form-control" id="exampleInputEmail1" name="nom" aria-describedby="emailHelp" >
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Prenom : <strong>'.$professeur['prenom'].'</strong></label>
                          <input  type="text" class="form-control" id="exampleInputEmail1" name="prenom" aria-describedby="emailHelp" >
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Mot de passe : <strong></strong></label>
                        <input type="text" class="form-control" name="mdp" id="exampleInputEmail1" aria-describedby="emailHelp">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Date de naissance : <strong>'.$professeur['anniversaire'].'</strong></label>
                        <input type="text"  id="datepicker" class="form-control" name="anniversaire" aria-describedby="emailHelp" >
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Photo : <strong>'.$professeur['photo'].'</strong></label>
                        <input type="file" class="form-control" name="photo2" id="exampleInputEmail1" aria-describedby="emailHelp" >
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <p style="text-align: center;"><input input type="submit" id="btValider" name="btValider" value="Valider" class="mx-sm-3 btn btn-primary"></p>
                  </div>
              </form>
            </div>
          </div>';
      }
    }
  }
  else{
     echo "<script type='text/javascript'>document.location.replace('index.php?page=professeur&selec');</script>";
  } 
}


else{

  //Afficher une alerte lorsque je modifie un professeur
  if(isset($_POST['btValider'])){

      $pseudo = $_POST['pseudo'];

      $professeur = new professeur($db);
      $Professeur = $professeur->selectOne($pseudo);
      $mail1 = $Professeur['mail'];
      $nom1 = $Professeur['nom'];
      $prenom1 = $Professeur['prenom'];
      $mdp1 = $Professeur['mdp'];
      $photo1 = $Professeur['photo'];
      $anniversaire1 = $Professeur['anniversaire'];

      $mail = $_POST['mail'];
      $nom = $_POST['nom'];
      $prenom = $_POST['prenom'];
      $mdp = sha1($_POST['mdp']);
      $anniversaire = $_POST['anniversaire'];


      if ($mail == NULL){
        $mail = $mail1;
      }
      if ($nom == NULL){
        $nom = $nom1;
      }
      if ($prenom == NULL){
        $prenom = $prenom1;
      }
      if ($mdp == NULL){
        $mdp = $mdp1;
      }
      if ($anniversaire == NULL){
        $anniversaire = $anniversaire1;
      }
      

      if (substr(strrchr($_FILES['photo2']['name'], '.'), 1)!=NULL){
        //extensions autorisées
        $extensions_ok = array('png', 'gif', 'jpg', 'jpeg');
        //taille maximun de la photo
        $taille_max = 500000;
        //répertoire ou sera stoker la photo
        $dest_dossier = '/var/www/html/kxence/assets/profils/professeurs/';

        //On verifie que la photo est dans le bon format
        //Si "NON" on affiche une alerte
        if( !in_array( substr(strrchr($_FILES['photo2']['name'], '.'), 1), $extensions_ok ) ){
          echo '
                <div class="col-lg-12">                           
                  <div class="card">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                          <i class="fa fa-ellipsis-v"></i>
                        </button>
                        <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
                          <a href="#" class="dropdown-item remove"><i class="fa fa-times"></i>Close</a>
                        </div>
                      </div>
                    </div>
                    <div  class="alert alert-danger" style="margin: 0px; padding: 18px; height: 60px; border-radius: 0px;" role="alert">Veuillez sélectionner un fichier de type png, gif ou jpg !</div>
                  </div>
                </div>';
        }
        //Si "OUI" ...
        else{
          //On vérifie que la photo respecte la taille max autorisé
          //Si "NON" on affiche une alerte
          if( file_exists($_FILES['photo2']['tmp_name'])&& (filesize($_FILES['photo2']['tmp_name']))>$taille_max){
            echo '
                <div class="col-lg-12">                           
                  <div class="card">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                          <i class="fa fa-ellipsis-v"></i>
                        </button>
                        <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
                          <a href="#" class="dropdown-item remove"><i class="fa fa-times"></i>Close</a>
                        </div>
                      </div>
                    </div>
                    <div  class="alert alert-danger" style="margin: 0px; padding: 18px; height: 60px; border-radius: 0px;" role="alert">Votre fichier doit faire moins de 500Ko !</div>
                  </div>
                </div>';
          }

          //Si "OUI" on affiche une alerte ...
          else{
            // copie du fichier
            $dest_fichier = basename($_FILES['photo2']['name']); // formatage nom fichier
            // enlever les accents
            $dest_fichier=strtr($dest_fichier,'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùú ûüýÿ','AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
            // remplacer les caractères autres que lettres, chiffres et point par _
            $dest_fichier = preg_replace('/([^.a-z0-9]+)/i', '_', $dest_fichier);
            // copie du fichier
            move_uploaded_file($_FILES['photo2']['tmp_name'],$dest_dossier . $dest_fichier);
            Unlink('var/www/html/kxence/assets/profils/professeurs/'.$photo1.'');
            $photo = $dest_fichier;

          }
        }
      }

      else{
         $photo = $photo1;
      }

            $professeur = new professeur($db);
            $nb = $professeur->updateAll($pseudo, $mail, $nom, $prenom, $mdp, $photo, $anniversaire);

              //Si il y a une erreur, on affiche une alerte     
              if ($nb!=1){
                echo '
                  <div class="col-lg-12">                           
                    <div class="card">
                      <div class="card-close">
                        <div class="dropdown">
                          <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                            <i class="fa fa-ellipsis-v"></i>
                          </button>
                          <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
                            <a href="#" class="dropdown-item remove"><i class="fa fa-times"></i>Close</a>
                          </div>
                        </div>
                      </div>
                    <div  class="alert alert-danger" style="margin: 0px; padding: 18px; height: 60px; border-radius: 0px;" role="alert">Erreur d\'insertion</div>
                  </div>
                </div>';
              }

              //Si il n'y a pas d'erreur, on affiche une alerte
              else{
                echo'
                  <div class="col-lg-12">                           
                    <div class="card">
                      <div class="card-close">
                        <div class="dropdown">
                          <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                            <i class="fa fa-ellipsis-v"></i>
                          </button>
                          <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
                            <a href="#" class="dropdown-item remove"><i class="fa fa-times"></i>Close</a>
                          </div>
                        </div>
                      </div>
                      <div class="alert alert-success" style="margin: 0px; padding: 18px; height: 60px; border-radius: 0px;" role="alert">Modification réussi</div>
                    </div>
                  </div>';
              }
    }

     












  //Afficher une alerte lorsque je supprime un professeur
  if(isset($_GET['suppr'])){
    $suppr = $_GET['suppr'];
    echo'
      <div class="col-lg-12">                           
        <div class="card">
          <div class="card-close">
            <div class="dropdown">
              <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                <i class="fa fa-ellipsis-v"></i>
              </button>
              <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
                <a href="#" class="dropdown-item remove"><i class="fa fa-times"></i>Close</a>
              </div>
            </div>
          </div>
          <div class="alert alert-success" style="margin: 0px; padding: 18px; height: 60px; border-radius: 0px;" role="alert">Suppression du compte <strong>'.$suppr.' </strong> réussi</div>
        </div>
      </div>';
  }


  //Afficher une alerte problèmme de sélection
  if(isset($_GET['selec'])){
    echo'
      <div class="col-lg-12">                           
        <div class="card">
          <div class="card-close">
            <div class="dropdown">
              <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                <i class="fa fa-ellipsis-v"></i>
              </button>
              <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
                <a href="#" class="dropdown-item remove"><i class="fa fa-times"></i>Close</a>
              </div>
            </div>
          </div>
          <div class="alert alert-danger" style="margin: 0px; padding: 18px; height: 60px; border-radius: 0px;" role="alert">Veuillez selectionner un professeur !</div>
        </div>
      </div>';
  }



  //Permet d'ajouter un professeur
  //Si il y a un fichier dans le champ photo
  if(isset($_FILES['photo'])){
    //extensions autorisées
    $extensions_ok = array('png', 'gif', 'jpg', 'jpeg');
    //taille maximun de la photo
    $taille_max = 500000;
    //répertoire ou sera stoker la photo
    $dest_dossier = '/var/www/html/kxence/assets/profils/professeurs/';

    //On verifie que la photo est dans le bon format
    //Si "NON" on affiche une alerte
    if( !in_array( substr(strrchr($_FILES['photo']['name'], '.'), 1), $extensions_ok ) ){
      echo '
            <div class="col-lg-12">                           
              <div class="card">
                <div class="card-close">
                  <div class="dropdown">
                    <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                      <i class="fa fa-ellipsis-v"></i>
                    </button>
                    <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
                      <a href="#" class="dropdown-item remove"><i class="fa fa-times"></i>Close</a>
                    </div>
                  </div>
                </div>
                <div  class="alert alert-danger" style="margin: 0px; padding: 18px; height: 60px; border-radius: 0px;" role="alert">Veuillez sélectionner un fichier de type png, gif ou jpg !</div>
              </div>
            </div>';
    }
    
    //Si "OUI" ...
    else{
      //On vérifie que la photo respecte la taille max autorisé
      //Si "NON" on affiche une alerte
      if( file_exists($_FILES['photo']['tmp_name'])&& (filesize($_FILES['photo']['tmp_name']))>$taille_max){
        echo '
            <div class="col-lg-12">                           
              <div class="card">
                <div class="card-close">
                  <div class="dropdown">
                    <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                      <i class="fa fa-ellipsis-v"></i>
                    </button>
                    <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
                      <a href="#" class="dropdown-item remove"><i class="fa fa-times"></i>Close</a>
                    </div>
                  </div>
                </div>
                <div  class="alert alert-danger" style="margin: 0px; padding: 18px; height: 60px; border-radius: 0px;" role="alert">Votre fichier doit faire moins de 500Ko !</div>
              </div>
            </div>';
      }

      //Si "OUI" on affiche une alerte ...
      else{
        // copie du fichier
        $dest_fichier = basename($_FILES['photo']['name']); // formatage nom fichier
        // enlever les accents
        $dest_fichier=strtr($dest_fichier,'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùú ûüýÿ','AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
        // remplacer les caractères autres que lettres, chiffres et point par _
        $dest_fichier = preg_replace('/([^.a-z0-9]+)/i', '_', $dest_fichier);
        // copie du fichier
        move_uploaded_file($_FILES['photo']['tmp_name'],$dest_dossier . $dest_fichier);
                  
        $photo = $dest_fichier;
        $professeur = new professeur($db);
                  
        $mail = $_POST['mail'];
        $nom = $_POST['nom'];
        $prenom = $_POST['prenom'];
        $mdp= sha1($_POST['mdp']);
        $anniversaire = $_POST['anniversaire'];
        
        $d = date('Y');
        $a = $d{2};
        $t = $d{3};
        $e = ''.$a.''.$t.'';
        
        $p = $prenom{0};
        $pseudo = 'p'.$p.''.$nom.''.$e.'';
        
        //On ajoute à la base de donnée                 
        $nb = $professeur->insertAll($pseudo, $mail, $nom, $prenom, $mdp, $photo, $anniversaire);          
        
        //Si il y a une erreur, on affiche une alerte     
        if ($nb!=1){
          echo '
            <div class="col-lg-12">                           
              <div class="card">
                <div class="card-close">
                  <div class="dropdown">
                    <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                      <i class="fa fa-ellipsis-v"></i>
                    </button>
                    <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
                      <a href="#" class="dropdown-item remove"><i class="fa fa-times"></i>Close</a>
                    </div>
                  </div>
                </div>
                <div  class="alert alert-danger" style="margin: 0px; padding: 18px; height: 60px; border-radius: 0px;" role="alert">Erreur d\'insertion</div>
              </div>
            </div>';
        }

        //Si il n'y a pas d'erreur, on affiche une alerte
        else{
          echo'
            <div class="col-lg-12">                           
              <div class="card">
                <div class="card-close">
                  <div class="dropdown">
                    <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                      <i class="fa fa-ellipsis-v"></i>
                    </button>
                    <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
                      <a href="#" class="dropdown-item remove"><i class="fa fa-times"></i>Close</a>
                    </div>
                  </div>
                </div>
                <div class="alert alert-success" style="margin: 0px; padding: 18px; height: 60px; border-radius: 0px;" role="alert">Création du compte <strong>'.$pseudo.' </strong> réussi</div>
              </div>
            </div>';
        }
      }
    }
  }

  //On appelle la fonction genererMdp() qui génére un mot de passe de 12 caractères
  $genpass = genererMdp(12);

  //Formulaire pour ajouter un professeur
  echo'
    <div class="col-lg-12">                           
      <div class="card">

        <div class="card-close">
          <div class="dropdown">
            <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
              <i class="fa fa-ellipsis-v"></i>
            </button>
            <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
              <a href="#" class="dropdown-item remove"><i class="fa fa-times"></i>Close</a>
            </div>
          </div>
        </div>

        <div class="card-header d-flex align-items-center">
            <h3 class="h4">Ajouter un Professeur</h3>
        </div>

        <form class="login-container" method="post" action= "index.php?page=professeur" enctype="multipart/form-data">
            <div class="row" style="margin-top: 18px; padding: 10px;">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="exampleInputEmail1">Mail</label>
                  <input required="" type="email" class="form-control" id="exampleInputEmail1" name="mail" aria-describedby="emailHelp"  >
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="exampleInputEmail1">Nom</label>
                  <input required="" type="text" class="form-control" id="exampleInputEmail1" name="nom" aria-describedby="emailHelp"  >
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="exampleInputEmail1">Prenom</label>
                    <input required="" type="text" class="form-control" id="exampleInputEmail1" name="prenom" aria-describedby="emailHelp"  >
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="exampleInputEmail1">Mot de passe</label>
                  <input required="" type="password" class="form-control" value="'.$genpass.'" name="mdp" id="exampleInputEmail1" aria-describedby="emailHelp"  >
                  <label for="optionsRadios2">(Le mot de passe générer par default est '.$genpass.')</label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="exampleInputEmail1">Date de naissance</label>
                  <input required="" type="text"  id="datepicker" class="form-control" name="anniversaire" aria-describedby="emailHelp">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="exampleInputEmail1">Photo</label>
                  <input required="" type="file" class="form-control" name="photo" id="exampleInputEmail1" aria-describedby="emailHelp"  >
                </div>
              </div>
            </div>
            <div class="form-group">
              <p style="text-align: center;"><input input type="submit" id="btAjouter" name="btAjouter" value="Ajouter" class="mx-sm-3 btn btn-primary"></p>
            </div>
        </form>
      </div>
    </div>';


            
  $professeur = new professeur($db);
  $listeprofesseur = $professeur->selectAll();

  //Lorsque on a appuyer sur le bouton supprimer
  if(isset($_POST['btSupprimer'])){
    //On récupere ce qui est cocher
    if (isset($_POST['cocher'])){
      $liste = $_POST['cocher'];

        $professeur = new professeur($db);
        //Pour le premier pseudo de la liste
        foreach($liste as $pseudo){
          $Professeur = $professeur->selectOne($pseudo);
          //on récupere le nom de la photo liée au pseudo
          $photo = $Professeur['photo'];
          //on supprime la photo
          Unlink('assets/profils/professeurs/'.$photo.'');
          //on supprime l'admin
          $nb = $professeur->deleteOne($pseudo);
          //On rafraichi la page, une nouvelle variable est placé en paramètre, elle récupére le pseudo de la personne supprimer
          //Pour ensuite afficher une alerte
          echo "<script type='text/javascript'>document.location.replace('index.php?page=professeur&suppr=".$pseudo."');</script>";
        }

      }
    else{
      echo "<script type='text/javascript'>document.location.replace('index.php?page=professeur&selec');</script>";
    } 
  }
  




  //CALCUL NOMBRE PROFESSEUR
  $sql = "SELECT count(pseudo) from professeur";
  $req2 = $db->query ($sql);
  $red3 = $req2->fetch();
  $totalprofesseur = ($red3[0]);



  //Afficher la liste des professeurs
  echo'
    <div class="col-lg-12">                           
      <div class="card">
        <div class="card-close">
          <div class="dropdown">
            <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
              <i class="fa fa-ellipsis-v"></i>
            </button>
            <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
              <a href="#" class="dropdown-item remove"><i class="fa fa-times"></i>Close</a>
            </div>
          </div>
        </div>

        <div class="card-header d-flex align-items-center">
            <h3 class="h4">Liste des Professeurs</h3>
        </div>

        <form method="post" action="index.php?page=professeur">
          <table class="table table-striped" id="myTable" class="tablesorter" cellspacing="1" >
            <thead>
              <tr id="titre">
                <th>Photo</th>
                <th>Pseudo</th>
                <th>Nom</th>
                <th>Prenom</th>
                <th>Mail</th>
                <th>Selectionner</th>';
              echo'
              </tr>
            </thead>
            <TBODY>
            ';

            //Pour chaque professeur, on affiche ...
            foreach($listeprofesseur as $unProfesseur){
                echo '<tr>';
                  echo '<td><img height="70px;" src="assets/profils/professeurs/'.utf8_encode($unProfesseur['photo'] ).'"></td>';
                  echo '<td>'.utf8_encode($unProfesseur['pseudo'] ).'</td>'; 
                  echo '<td>'.utf8_encode($unProfesseur['nom'] ).'</td>'; 
                  echo '<td>'.utf8_encode($unProfesseur['prenom'] ).'</td>'; 
                  echo '<td>'.utf8_encode($unProfesseur['mail'] ).'</td>';

                  //Permet de toujours avoir un professeur au minimun
                  echo '<td> <input type="radio" name="cocher[]" value="'.$unProfesseur['pseudo'].'"> </td>';
                echo '</tr>';
            }

          echo '
            </TBODY>
              </table>
               <p style="text-align: center;">
                 <input input type="submit" id="btSupprimer" name="btSupprimer" value="Supprimer" class="mx-sm-3 btn btn-primary">
                 <input input type="submit" id="btModifier" name="btModifier" value="Modifier" class="mx-sm-3 btn btn-primary">
               </p>
              </form>
            </div>
          </div>';

  }
}













function listeprof($db){

  $professeur = new professeur($db);
  $listeprof = $professeur->selectAll();

  echo'<div class="col-lg-12">                           
          <div class="card">

            <div class="card-close">
              <div class="dropdown">
                <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                  <i class="fa fa-ellipsis-v"></i>
                </button>
                <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
                  <a href="#" class="dropdown-item remove"><i class="fa fa-times"></i>Close</a>
                </div>
              </div>
            </div>

            <div class="card-header d-flex align-items-center">
                <h3 class="h4">Liste des Professeurs</h3>
            </div>

            <table class="table table-striped" >
              <thead>
                <tr id="titre">
                  <th>Photo</th>
                  <th>Pseudo</th>
                  <th>Nom</th>
                  <th>Prenom</th>
                  <th>Mail</th>
                </tr>
              </thead>
              <TBODY>';
              foreach($listeprof as $unProf){
                echo '<tr>';
                    echo '<td><img height="70px;" src="assets/img/profils/professeurs/'.utf8_encode($unProf['photo'] ).'"></td>';
                    echo '<td>'.utf8_encode($unProf['pseudo'] ).'</td>'; 
                    echo '<td>'.utf8_encode($unProf['nom'] ).'</td>'; 
                    echo '<td>'.utf8_encode($unProf['prenom'] ).'</td>'; 
                    echo '<td>'.utf8_encode($unProf['mail'] ).'</td>'; 
                echo '</tr>';
              }
        echo '</TBODY>
            </table>
          </div>
        </div>';
}

?>