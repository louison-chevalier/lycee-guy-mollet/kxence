 <?php

function reseau(){

if(isset($_POST['reboot'])){
    $reboot = 'sudo reboot';
    shell_exec($reboot);
}

if(isset($_POST['poweroff'])){
    $reboot = 'sudo poweroff';
    shell_exec($reboot);
}


 echo '
        <div class="content-inner">
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Réseau</h2>
            </div>
          </header>
          
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                
                <div class="col-lg-12">                           
                  <div class="card">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                        <div aria-labelledby="closeCard" class="dropdown-menu has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
                      </div>
                    </div>

                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">OUTILS</h3>
                    </div>
                    
                    <div class="line"></div>
                    
                    <form class="login-container" method="post" action= "index.php?page=reseau" enctype="multipart/form-data">
                        <div class="form-group row" style="text-align:center; margin-top:50px; ">
                            <label class="col-sm-6 form-control-label" style="padding-top:7px;">Redémarer le serveur<br><small class="text-primary">Le site ne sera plus disponile quelques instants !</small></label>
                            <p class="col-sm-5" style="text-align:center;"><input input type="submit" id="reboot" name="reboot" value="Reboot" class="btn btn-primary"></p>
                        </div>
                    </form>
                    
                    <div class="line"></div>
                    
                    <form class="login-container" method="post" action= "index.php?page=reseau" enctype="multipart/form-data">
                        <div class="form-group row" style="text-align:center; margin-top:50px; ">
                            <label class="col-sm-6 form-control-label" style="padding-top:7px;">Éteindre le serveur<br><small class="text-primary">Le site ne sera plus disponile !</small></label>
                            <p class="col-sm-5" style="text-align:center;"><input input type="submit" id="poweroff" name="poweroff" value="Power off" class="btn btn-primary"></p>
                        </div>
                    </form>
                    
                    <div class="line"></div>
                    </div>
                </div>
          </section>
          ';
          
          }

?>