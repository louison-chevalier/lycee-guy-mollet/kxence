<?php

function getPage(){
            
    // On déclare dans le tableau $lesPages, une clé et une valeur
    // La clé est ajoutType, la valeur est ajoutType
	
    
    // 0 voudra dire « tout le monde »

    $lesPages['Login'] = "Login;0";
    $lesPages['Passwd'] = "Passwd;0";
        $lesPages['co'] = "co;2";



     // 2 voudra dire « admin »
    $lesPages['Accueil'] = "Accueil;2";
    $lesPages['profiladmin'] = "profiladmin;2";
    $lesPages['modifprofiladmin'] = "modifprofiladmin;2";
    $lesPages['reseau'] = "reseau;2";
    $lesPages['filtre'] = "filtre;2";
    $lesPages['gestionconnect'] = "gestionconnect;2";
    $lesPages['administrateur'] = "administrateur;2";
    $lesPages['documents'] = "documents;2";
    $lesPages['professeur'] = "professeur;2";
    $lesPages['classe'] = "classe;2";
    $lesPages['admin'] = "admin;2";
    $lesPages['eleves'] = "eleves;2";
    $lesPages['professeur'] = "professeur;2";
    $lesPages['mentions'] = "mentions;2";
    $lesPages['apropos'] = "apropos;2";

     // 1 voudra dire « prof »

        
    
	// regarde si en mémoire, une variable « page » vient d'un lien ($_GET)
	if(isset($_GET['page'])){
	   // Nous mettons dans la variable $page, la valeur qui a été passée dans le lien
	   $page = $_GET['page'];
    }

	else{
	   // S'il n'y a rien en mémoire, nous lui donnons la valeur « accueil » afin de lui afficher une page
	   //par défaut
	   $page = 'Accueil';
	}


	// Nous regardons si la page passée en paramètre est une clé du tableau $lesPages
	if (!isset($lesPages[$page])){
	   // Nous rentrons ici si cela n'existe pas, ainsi nous redirigeons l'utilisateur sur la page d'accueil
	   $page = 'Accueil';
    }

	// Nous explosons la valeur du tableau correspondant à la clé
    // C'est le ; qui nous sert de repère
	$explose = explode(";",$lesPages[$page]);
    // Je récupère le rôle de la personne connectée (O pour tout le monde, 1 pour l’administrateur)
    $rolePage = $explose[1];
    // Si la page est pour tout le monde

    if($rolePage == 0){
        // le contenu est directement affecté
        $contenu = $explose[0];
    }

    else{
        // Je regarde si la personne est connectée
        if (isset($_SESSION['pseudo'])){
            // Je récupère le rôle de la personne s’il existe
            if (isset($_SESSION['role'])){
                // Je regarde si le rôle de la personne correspond au rôle qu’il faut avoir pour lire
                // la page
                if($rolePage==$_SESSION['role']){
                    // C’est le cas donc je mets le contenu
                    $contenu = $explose[0];
                }
                else{
                    // Il n’a pas les droits donc je mets la page d’accueil
                    $contenu = 'Accueil';
                }
            }
        } 
        else{
            // La personne n’est pas connectée donc on lui donne la page d’accueil
            $contenu = 'Login';
        }
    
}
return $contenu;
}


?>
