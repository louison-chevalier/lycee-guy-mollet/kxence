<?php

/******************************************************************************/
/******************* EN TÊTE QUI S'APPLIQUE SUR TOUS LE SITE ******************/
/******************************************************************************/

function entete(){
    
    echo'
    <!DOCTYPE html>
    <head>
    <meta charset="utf-8">
    <title>KXENCE - Administration</title>
    
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <!-- Google fonts - Roboto -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">



    <!-- theme stylesheet-->
    <link rel="stylesheet" href="assets/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="assets/css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="assets/img/favicon.ico">


    <!-- Font Awesome CDN-->
    <!-- you can replace it by local Font Awesome-->
    <script rel="stylesheet" src="assets/js/99347ac47f.js"></script>


    <!-- Font Icons CSS-->
    <link rel="stylesheet" href="https://file.myfontastic.com/da58YPMQ7U5HY8Rb6UxkNf/icons.css">


    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  

     <!-- JQUERY-->
    <link rel="stylesheet" href="assets/css/jquery-ui.css"/> 
    <link rel="stylesheet" href="assets/css/multi-select.css"/>
    
</head>';
}



/******************************************************************************/
/********************* MENU QUI S'APPLIQUE SUR TOUS LE SITE *******************/
/******************************************************************************/

function menu($db){

$role = $_SESSION['role'];
$pseudoconnect = $_SESSION['pseudo'];

echo'<div class="page home-page">
      <header class="header">
        <nav class="navbar">
          <div class="container-fluid">
            <div class="navbar-holder d-flex align-items-center justify-content-between">
              <div class="navbar-header">
                <a href="index.php?page=" class="navbar-brand">
                  <div class="brand-text brand-big hidden-lg-down"><span>Administration - </span><strong> KXENCE</strong></div>
                  <div class="brand-text brand-small"><strong>KXENCE</strong></div>
                </a>
                <a id="toggle-btn" href="index.php?page=" class="menu-btn active"><span></span><span></span><span></span></a>
              </div>
              
              <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                <li class="nav-item dropdown"> <a id="messages" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link"><i class="fa fa-slideshare"></i><span class="badge bg-orange"></span></a>
                  <ul aria-labelledby="notifications" class="dropdown-menu">
                   <li><strong style="padding-left:10px;">Dernières connexions</strong></li>';

                  $accueil = new accueil($db);
                  $derniereco = $accueil->derniereCo();
                  
                  foreach($derniereco as $utilisateur){
                      
                    $pseudo = $utilisateur['pseudo'];
                    $date = $utilisateur['date'];
                    
                    $accueil = new accueil($db);
                    $accueil = $accueil->selectOne($pseudo);
                    
                    $roleaccueil = $accueil['role'];
                    $date1 = new DateTime($date);
                    $date2 = new DateTime();
                    
                    
                    if ($roleaccueil == "admins"){
                        $accueil = new accueil($db);
                        $adminaccueil = $accueil->selOne($pseudo);
                        $photo = $adminaccueil['photo'];
                        $liens = "assets/profils/admins/".$photo."";
                    }
                    
                    if ($roleaccueil == "professeur"){
                        $accueil = new accueil($db);
                        $profaccueil = $accueil->seOne($pseudo);
                        $photo = $profaccueil['photo'];
                        $liens = "assets/profils/professeurs/".$photo."";
                    }
                    
                    
                    echo'<li><a rel="nofollow" href="" class="dropdown-item d-flex"> 
                        <div class="msg-profile"> <img src="'.$liens.'" alt="..." class="img-fluid rounded-circle"></div>
                        <div class="msg-body">
                          <h3 class="h5">'.$pseudo.'</h3><span>';
                                echo $date1->diff($date2)->format("%d jours %H:%i:%s");
                                echo'</span>
                        </div></a></li>';
                  }
                  
                    echo'
                  </ul>
                </li>
                

                        
                <li class="nav-item"><a href="index.php?page=Login&deco=oui"  class="nav-link logout">Déconnexion<i class="fa fa-times-rectangle-o"></i></a></li>
              </ul>
            </div>
          </div>
        </nav>
      </header>

      <div class="page-content d-flex align-items-stretch">
        <nav class="side-navbar">
          <div class="sidebar-header d-flex align-items-center">';
      
      /******************************************************************************/
      /************SI LE COMPTE CONNECTÉ EST UN PROFESSEUR ALORS ********************/
      /******************************************************************************/
      if ($role ==1) {
          $professeur = new professeur($db);
          $unProfesseur = $professeur->selectOne($pseudoconnect);

          echo '<div class="avatar">
                  <img src="assets/profils/professeurs/'.$unProfesseur['photo'].'" class="img-fluid rounded-circle">
                </div>
                <div class="title">
                  <h1 class="h4" style="text-transform: uppercase;">';
                  echo $pseudoconnect;
          echo '  </h1>';
          echo '  <p>Professeur</p>
                </div>
            </div>
            <ul class="list-unstyled">
              <li> <a href="index.php?page=Accueil"><i class="icon-home"></i>Accueil</a></li>
              <li> <a href="index.php?page=profiladmin"> <i class="icon-grid"></i>Mon Profil</a></li>
            </ul>

            <span class="heading">Utilisateurs</span>
            <ul class="list-unstyled">
              <li> <a href="index.php?page=administrateur"> <i class="icon-user"></i>Admin</a></li>
              <li> <a href="index.php?page=professeur"> <i class="icon-user"></i>Professeurs</a></li>
              <li> <a href="index.php?page=eleves"> <i class="icon-user"></i>Élèves</a></li>
              <li> <a href="index.php?page=classe"> <i class="icon-user"></i>Classes</a></li>
            </ul>
            <span class="heading">Services</span>
            <ul class="list-unstyled">
              <li> <a href="index.php?page=reseau"> <i class="icon-flask"></i>Réseau </a></li>
              <li> <a href="#"> <i class="icon-screen"></i>Demo </a></li>
            </ul>
            <span class="heading">Aide</span>
            <ul class="list-unstyled">
              <li> <a href="index.php?page=Login"> <i class="icon-flask"></i>Demo </a></li>
              <li> <a href="index.php?page=Accueil"> <i class="icon-screen"></i>Demo </a></li>
            </ul>
            </nav>';
      }


      /******************************************************************************/
      /************SI LE COMPTE CONNECTÉ EST UN ADMINISTRATEUR ALORS ****************/
      /******************************************************************************/
      if ($role ==2) {

          $admin = new admin($db);
          $unAdmin = $admin->selectOne($pseudoconnect);

          echo '<div class="avatar">
                  <img src="assets/profils/admins/'.$unAdmin['photo'].'" class="img-fluid rounded-circle">
                </div>
                <div class="title">
                  <h1 class="h4" style="text-transform: uppercase;">';
                  echo $pseudoconnect;
          echo '  </h1>';
          echo '  <p>Admin</p>
                </div>
            </div>
            <ul class="list-unstyled">
              <li> <a href="index.php?page=Accueil"><i class="fa fa-home"></i>Accueil</a></li>
              <li> <a href="index.php?page=profiladmin"> <i class="fa fa-address-card-o"></i>Mon Profil</a></li>
              <li> <a href="index.php?page=documents"> <i class="fa fa-folder-open-o"></i>Mes documents</a></li>
            </ul>

            <span class="heading">Utilisateurs</span>
            <ul class="list-unstyled">
              <li> <a href="index.php?page=administrateur"> <i class="fa fa-user-secret"></i>Admin</a></li>
              <li> <a href="index.php?page=professeur"> <i class="fa fa-user"></i>Professeurs</a></li>
              <li> <a href="index.php?page=eleves"> <i class="fa fa-user"></i>Élèves</a></li>
              <li> <a href="index.php?page=classe"> <i class=" 	fa fa-group"></i>Classes</a></li>
            </ul>
            <span class="heading">Services</span>
            <ul class="list-unstyled">
              <li> <a href="index.php?page=reseau"> <i class=" 	fa fa-sitemap"></i>Réseau </a></li>
              <li> <a href="index.php?page=filtre"> <i class="fa fa-sliders"></i>Filtre accès</a></li>
              <li> <a href="index.php?page=gestionconnect"> <i class="fa fa-gears"></i>Connexions</a></li>
            </ul>
            <span class="heading">Aide</span>
            <ul class="list-unstyled">
              <li> <a href="index.php?page=apropos"> <i class="fa fa-question-circle-o"></i>A propos</a></li>
              <li> <a href="index.php?page=mentions"> <i class=" 	fa fa-tasks"></i>Mentions légales </a></li>
            </ul>
            </nav>';
      }
}
        
   
/******************************************************************************/
/***************** BAS DE PAGE QUI S'APPLIQUE SUR TOUS LE SITE ****************/
/******************************************************************************/

function bas(){
    
  echo '

      <!-- Javascript files-->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
      <script src="assets/js/tether.min.js"></script>
      <script src="assets/js/bootstrap.min.js"></script>

    <script src="assets/js/jquery-1.9.1.js"></script>
    <script src="assets/js/jquery-ui-1.10.2.custom.js"></script>
    <script type="text/javascript" src="assets/js/calendrier.js"></script>
    
    <script type="text/javascript" src="assets/js/table/moncode.js"></script>
    <script type="text/javascript" src="assets/js/table/jquery.tablesorter.js"></script>
      
    <script src="assets/js/jquery.multi-select.js"></script>
    <script src="assets/js/select.js"></script>

      <script src="assets/js/jquery.cookie.js"> </script>
      <script src="assets/js/jquery.validate.min.js"></script>

      <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
      <script src="assets/js/charts-home.js"></script>
      <script src="assets/js/front.js"></script>
      
    <script src="assets/js/refresh.js"></script>





    </body>
  </html>';
}



?>
